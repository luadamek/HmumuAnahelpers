# HmumuAnahelpers
This package is based on the [xAODAnaHelpers (xAH)](https://github.com/UCATLAS/xAODAnaHelpers) package, thus I strongly recommend you check out the [xAH documentation first](https://xaodanahelpers.readthedocs.io/en/latest/).

For questions please contact: lukas.adamek[at]cern.ch

First make sure that you're working inside of a singularity conainer
```
setupATLAS -c slc6+batch
```

```
mkdir myAnalysis; cd myAnalysis
mkdir source && mkdir run && mkdir build && mkdir run/results
cd source
git clone http://github.com/UCATLAS/xAODAnaHelpers xAODAnaHelpers
cd xAODAnaHelpers && git checkout aaf7fc3fde9819bcb5cc3737df0226e275110671 && cd ..
git clone https://:@gitlab.cern.ch:8443/luadamek/HmumuAnahelpers.git
asetup AnalysisBase,21.2.84,here
cd ../build
cmake ../source && make
source x86*/*.sh
```

Whenever you log back in
```
setupATLAS -c slc6+batch
cd myAnalysis/source
asetup --restore
cd ../build
source x86*/*.sh
cd ..
```

## Running locally in Release 21
The analysis is configured using dictionaries inside the file configs/generalConfig.py. This dictionary sets various important flags, which are eventually used in the function setupHmumuAnalysis in configs/config_Hmumu.py. There are stark differences in sumitting jobs for mc16a and mc16d, so there are year-specific configuration dictionaries contained in configs/generalConfig.py. These dictionaries are also used in the function setupHmumuAnalysis. The configuration scripts for submitting jobs are configs/config_Hmumu_mc16a.py and configs/config_Hmumu_cm16d.py. These two steering scripts pass the correct configuration dictionaries to the setupHmumuAnalysis function, and configure the Hmumu analysis. There are examples of configurations with tighter selections, or with debugging options. They can be found in the files like configs/config_Hmumu_mc16a_signal.py, for example. config_Hmumu_mc16a_signal.py sets an additional flag "applySignalRegionSelection", to selection events where the leading opposite charged muons have invariant mass between 108 GeV and 170 GeV. 

Download the nTup pileup files needed for running over the HIGG1D2 files
```
cd source/HmumuAnahelpers
source download_ntup_pileup.sh
cd ../build
make
```

An example of how to run a local job is shown below. The config file used here is mc16d, because it is an mc16d file that is used as input.
```
lsetup rucio
voms-proxy-init --voms atlas
cd $TestArea/../run
rucio download  mc16_13TeV:DAOD_HIGG1D2.20317213._000001.pool.root.1
xAH_run.py --files mc16_13TeV/DAOD_HIGG1D2.20317213._000001.pool.root.1 --nevents 500 --config $TestArea/HmumuAnahelpers/configs/config_Hmumu_mc16d.py --submitDir test --force direct
```

An example of how to run a grid job is shown below. The list of files contains the fill list of files to run over. A complete list of files for the DAOD_HIGG4D1 derivaiton can be found in the data folder.

## Submitting Grid Jobs in Release 21
```
lsetup panda
voms-proxy-init --voms atlas
cd $TestArea/../run
source $WorkDir_DIR/setup.sh
python $TestArea/HmumuAnahelpers/source/HmumuAnahelpers/scripts/submit_grid.py --user YOURUSERNAME --submitDir test --FileList $TestArea/HmumuAnahelpers/data/samples/v01/sample_lists/test_single_signal_sample.txt --config $TestArea/source/HmumuAnahelpers/configs/config_Hmumu_mc16d.py --descriptor test
```
