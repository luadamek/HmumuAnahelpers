#R21 Hmumu Analysiw
from sys import path as sys_path
from os import path as os_path
WorkDir_DIR = os_path.expandvars('$WorkDir_DIR')
sys_path.insert(1, WorkDir_DIR + "/bin")

from xAH_config import xAH_config
from generalConfig import generalConfig

def setupHmumuAnalysis(c, generalConfig, yearSpecificConfig):
    #all information needed for triggers
    triggers = yearSpecificConfig["triggers"]
    triggerSF = yearSpecificConfig["triggerSF"]

    GRL = yearSpecificConfig["GRL"]
    LumiCalcFile = yearSpecificConfig["LumiCalcFile"]
    PRW_Files = yearSpecificConfig["PRW_Files"]

    #information about btagging
    BTaggingCorrFile = generalConfig["BTaggingCorrFile"]
    MuonPt = generalConfig["MuonPt"]
    MuonEta = generalConfig["MuonEta"]
    MuonQual = generalConfig["MuonQual"]
    ElectronQual = generalConfig["ElectronQual"]
    MuonIsoWP = generalConfig["MuonIsoWP"]
    ElectronIsoWP = generalConfig["ElectronIsoWP"]
    Muon_d0sig_max = generalConfig["Muon_d0sig_max"]
    Muon_z0sintheta_max = generalConfig["Muon_z0sintheta_max"]
    ElectronPt = generalConfig["ElectronPt"]
    ElectronEta = generalConfig["ElectronEta"]
    Electron_d0sig_max = generalConfig["Electron_d0sig_max"]
    Electron_z0sintheta_max = generalConfig["Electron_z0sintheta_max"]

    JetJVTWP = generalConfig["JetJVTWP"]
    doJVT = generalConfig["doJVT"]

    JetfJVTWP = generalConfig["JetfJVTWP"]
    dofJVT = generalConfig["dofJVT"]

    #information about the cuts to be applied
    applyLeadingMuonPtSelection = generalConfig["applyLeadingMuonPtSelection"]
    minPtCut = generalConfig["minPtCut"]
    applyTwoMuonsSelection = generalConfig["applyTwoMuonsSelection"]
    applyTwoLeptonsOppositeChargeSelection = generalConfig["applyTwoLeptonsOppositeChargeSelection"]
    applyTwoMuonsOppositeChargeSelection = generalConfig["applyTwoMuonsOppositeChargeSelection"]
    applySignalRegionSelection = generalConfig["applySignalRegionSelection"]
    applyZJetsRegionSelection = generalConfig["applyZJetsRegionSelection"]
    applyOneBTagSelection = generalConfig["applyOneBTagSelection"]
    msgLevel = generalConfig["msgLevel"]
    doTruthLevel = generalConfig["doTruthLevel"]
    doSystematics = generalConfig["doSystematics"]
    doJetSystematicsOnly = generalConfig["doJetSystematicsOnly"]
    doNonJetSystematicsOnly = generalConfig["doNonJetSystematicsOnly"]

    triggerSelection2015 = generalConfig["triggerSelection2015"]
    triggerSelection201620172018 = generalConfig["triggerSelection201620172018"]

    msgLevel='info'
    ''' Set up all the algorithms '''
    BasicEventSelection =            {"m_name": "BasicEventSelection",
                                     "m_applyGRLCut": True,
                                     "m_GRLxml":GRL,
                                     "m_lumiCalcFileNames": LumiCalcFile,
                                     "m_PRWFileNames": PRW_Files,
                                     "m_doPUreweighting": True,
                                     "m_doPUreweightingSys": False,
                                     "m_applyPrimaryVertexCut": True,
                                     "m_applyEventCleaningCut": True, #This cut is set in the HmumuEventSelection class
                                     "m_applyCoreFlagsCut": True,
                                     "m_applyTriggerCut": False,
                                     "m_triggerSelection": triggers,
                                     "m_storeTrigDecisions": True,
                                     "m_PVNTrack": 2,
                                     "m_useMetaData": True,
                                     "m_checkDuplicatesData": True,
                                     "m_checkDuplicatesMC": False}
    c.setalg("BasicEventSelection", BasicEventSelection)

    HmumuEventSelection =          {"m_name": "HmumuEventSelection",
                                      "m_triggerSelection201620172018": triggerSelection201620172018,
                                      "m_triggerSelection2015": triggerSelection2015,
                                      "m_msgLevel": msgLevel,
                                      }
    c.setalg("HmumuEventSelection", HmumuEventSelection)

    '''set up muon calibration'''
    HmumuMuonCalibrator =       {"m_name": "HmumuMuonCalibrator",
                                "m_inContainerName": "Muons",
                                "m_outContainerName": "CalibMuons",
                                "m_sort": True,
                                "m_msgLevel":msgLevel,
                                }

    if doNonJetSystematicsOnly:
        HmumuMuonCalibrator["m_systName"] = "All"
        HmumuMuonCalibrator["m_outputAlgoSystNames"] = HmumuMuonCalibrator["m_name"] + "_syst"
    c.setalg("MuonCalibrator", HmumuMuonCalibrator)

    '''set up jet calibration'''
    HmumuJetCalibrator =        {"m_name": "HmumuJetCalibrator",
                                "m_inContainerName": "AntiKt4EMTopoJets",
                                "m_jetAlgo": "AntiKt4EMTopo",
                                "m_outContainerName": "CalibJets",
                                "m_calibConfigData":"JES_data2017_2016_2015_Consolidated_EMTopo_2018_Rel21.config",
                                "m_calibConfigFullSim": "JES_data2017_2016_2015_Consolidated_EMTopo_2018_Rel21.config",
                                "m_calibSequence": "JetArea_Residual_EtaJES_GSC",
                                "m_sort": True,
                                "m_calculatefJVT":True,
                                "m_fJVTWorkingPoint":JetfJVTWP,
                                "m_doCleaning": True,
                                "m_forceInsitu": True,
                                "m_forceSmear" : True,
                                "m_jetCleanCutLevel" : "LooseBad",
                                "m_jetCleanUgly" : False,
                                "m_msgLevel":msgLevel,
                                }
    if doJetSystematicsOnly:
        HmumuJetCalibrator["m_outputAlgo"] = HmumuJetCalibrator["m_name"] + "_syst"
        HmumuJetCalibrator["m_systVal"] = 1.0
        HmumuJetCalibrator["m_uncertConfig"] = "rel21/Fall2018/R4_CategoryReduction_SimpleJER.config"
        HmumuJetCalibrator["m_systName"] = "All"
    c.setalg("JetCalibrator", HmumuJetCalibrator)

    '''set up electron calibration'''
    #Read about the options for uncertainties here: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/ElectronPhotonFourMomentumCorrection#Systematics
    HmumuElectronCalibrator =    {"m_name": "HmumuElectronCalibrator",
                                 "m_inContainerName": "Electrons",
                                 "m_outContainerName": "CalibElectrons",
                                 "m_esModel": "es2017_R21_v1",
                                 "m_decorrelationModel": "1NP_v1",
                                 "m_sort": True,
                                 "m_msgLevel":msgLevel,
                                 }
    if doNonJetSystematicsOnly:
        HmumuElectronCalibrator["m_outputAlgoSystNames"] = HmumuElectronCalibrator["m_name"] + "_syst"
        HmumuElectronCalibrator["m_systName"] = "All"
    c.setalg("ElectronCalibrator", HmumuElectronCalibrator)

    '''set up the muon selection'''
    HmumuMuonSelection =        {"m_name": "HmumuMuonSelection",
                                "m_inContainerName": "CalibMuons",
                                "m_createSelectedContainer": True,
                                "m_decorateSelectedObjects":False,
                                "m_pT_min": MuonPt,
                                "m_eta_max": MuonEta,
                                "m_d0sig_max": Muon_d0sig_max,
                                "m_z0sintheta_max": Muon_z0sintheta_max,
                                "m_muonQualityStr": MuonQual,
                                "m_MinIsoWPCut": MuonIsoWP,
                                #"m_IsoWPList":"FCTightTrackOnly_FixedRad,FCLoose_FixedRad,FCTight_FixedRad,FixedCutPflowTight,FixedCutPflowLoose",
                                "m_removeEventBadMuon": False,
                                "m_outContainerName" : "PreORMuons",
                                "m_singleMuTrigChains": triggers,
                                "m_msgLevel":msgLevel,
                                }
    if doNonJetSystematicsOnly:
        HmumuMuonSelection["m_inputAlgoSystNames"] = HmumuMuonCalibrator["m_outputAlgoSystNames"]
        HmumuMuonSelection["m_outputAlgoSystNames"] = HmumuMuonSelection["m_name"] + "_syst"
    c.setalg("MuonSelector",HmumuMuonSelection)

    HmumuJetSelection =         {"m_name": "HmumuJetSelection",
                                "m_inContainerName": "CalibJets",
                                "m_pT_min": 25000,
                                "m_eta_max": 4.5,
                                "m_SFFileJVT": "JetJvtEfficiency/Moriond2018/JvtSFFile_EMTopoJets.root",
                                "m_SFFilefJVT": "JetJvtEfficiency/Moriond2018/fJvtSFFile.root",
                                "m_doJVT": doJVT,
                                "m_dofJVT": dofJVT,
                                "m_WorkingPointJVT": JetJVTWP,
                                "m_WorkingPointfJVT": JetfJVTWP,
                                "m_outContainerName": "NoForwardSelectionJets",
                                "m_createSelectedContainer": True,
                                "m_decorateSelectedObjects": False,
                                "m_outputSystNamesJVT": "JVTSystList",
                                "m_outputSystNamesfJVT": "fJVTSystList",
                                "m_msgLevel":msgLevel,
                                }
    JetSystListJVT = ""
    if doJetSystematicsOnly:
        HmumuJetSelection["m_inputAlgo"] = HmumuJetCalibrator["m_outputAlgo"]
        HmumuJetSelection["m_outputAlgo"] = HmumuJetSelection["m_name"] + "_syst"
    if doNonJetSystematicsOnly:
        HmumuJetSelection["m_systValJVT"] = 1.0
        HmumuJetSelection["m_systNameJVT"] = "All"
        JetSystListJVT = HmumuJetSelection["m_outputSystNamesJVT"] + "_JVT_" + JetJVTWP

    c.setalg("JetSelector",HmumuJetSelection)

    HmumuForwardJetSelection =   {"m_name": "HmumuForwardJetSelectionAlgo",
                                 "m_inContainerName": "NoForwardSelectionJets",
                                 "m_eta_min": 2.4,
                                 "m_eta_max": 4.5,
                                 "m_pt_min": 30e3,
                                 "m_outContainerName": "PreORJets",
                                 "m_msgLevel":msgLevel
                                 }
    if doJetSystematicsOnly:
        HmumuForwardJetSelection["m_inputAlgoSystNames"] = HmumuJetCalibrator["m_outputAlgo"]
        HmumuForwardJetSelection["m_outputAlgoSystNames"] =  HmumuForwardJetSelection["m_name"] + "_syst"
    c.setalg("HmumuForwardJetSelection", HmumuForwardJetSelection)

    HmumuElectronSelection =   {"m_name": "HmumuElectronSelection",
                                "m_inContainerName": "CalibElectrons",
                                "m_createSelectedContainer": True,
                                "m_decorateSelectedObjects":False,
                                "m_d0sig_max": Electron_d0sig_max,
                                "m_z0sintheta_max": Electron_z0sintheta_max,
                                "m_pT_min": ElectronPt,
                                "m_eta_max": ElectronEta,
                                "m_doLHPID": True,
                                "m_doLHPIDcut": True,
                                "m_LHOperatingPoint": ElectronQual,
                                "m_MinIsoWPCut": ElectronIsoWP,
                                #"m_IsoWPList":"FCLoose,FCTight,Gradient,FCHighPtCaloOnly",
                                "m_vetoCrack": True,
                                "m_readIDFlagsFromDerivation": True,
                                "m_outContainerName" : "PreORElectrons",
                                "m_msgLevel":msgLevel,
                                }
    if doNonJetSystematicsOnly:
        HmumuElectronSelection["m_inputAlgoSystNames"] = HmumuElectronCalibrator["m_outputAlgoSystNames"]
        HmumuElectronSelection["m_outputAlgoSystNames"] = HmumuElectronSelection["m_name"] + "_syst"
    c.setalg("ElectronSelector", HmumuElectronSelection)

    HmumuBtaggingEffCorrectorFixedCutBEff_60 = {"m_name": "HmumuBtaggingEffCorrectorFixedCutBEff_60",
                                "m_inContainerName": "PreORJets",
                                "m_operatingPt": "FixedCutBEff_60",
                                "m_taggerName": "MV2c10",
                                "m_corrFileName": BTaggingCorrFile,
                                "m_msgLevel":msgLevel,
                                }
    JetSystListBJet = ""
    if doJetSystematicsOnly:
        HmumuBtaggingEffCorrectorFixedCutBEff_60["m_inputAlgo"] = HmumuForwardJetSelection["m_outputAlgoSystNames"]
    if doNonJetSystematicsOnly:
        HmumuBtaggingEffCorrectorFixedCutBEff_60["m_systName"] = "All"
        HmumuBtaggingEffCorrectorFixedCutBEff_60["m_systVal"] = 1.0
        HmumuBtaggingEffCorrectorFixedCutBEff_60["m_outputSystName"] = "SystListBJet"
    c.setalg("BJetEfficiencyCorrector", HmumuBtaggingEffCorrectorFixedCutBEff_60)

    HmumuBtaggingEffCorrectorFixedCutBEff_70 = {"m_name": "HmumuBtaggingEffCorrectorFixedCutBEff_70",
                                "m_inContainerName": "PreORJets",
                                "m_operatingPt": "FixedCutBEff_70",
                                "m_taggerName": "MV2c10",
                                "m_corrFileName": BTaggingCorrFile,
                                "m_msgLevel":msgLevel,
                                }

    JetSystListBJet = ""
    if doJetSystematicsOnly:
        HmumuBtaggingEffCorrectorFixedCutBEff_70["m_inputAlgo"] = HmumuForwardJetSelection["m_outputAlgoSystNames"]
    if doNonJetSystematicsOnly:
        HmumuBtaggingEffCorrectorFixedCutBEff_70["m_systName"] = "All"
        HmumuBtaggingEffCorrectorFixedCutBEff_70["m_systVal"] = 1.0
        HmumuBtaggingEffCorrectorFixedCutBEff_70["m_outputSystName"] = "SystListBJet"
    c.setalg("BJetEfficiencyCorrector", HmumuBtaggingEffCorrectorFixedCutBEff_70)

    HmumuBtaggingEffCorrectorFixedCutBEff_77 = {"m_name": "HmumuBtaggingEffCorrectorFixedCutBEff_77",
                                "m_inContainerName": "PreORJets",
                                "m_operatingPt": "FixedCutBEff_77",
                                "m_taggerName": "MV2c10",
                                "m_corrFileName": BTaggingCorrFile,
                                "m_msgLevel":msgLevel,
                                }

    JetSystListBJet = ""
    if doJetSystematicsOnly:
        HmumuBtaggingEffCorrectorFixedCutBEff_77["m_inputAlgo"] = HmumuForwardJetSelection["m_outputAlgoSystNames"]
    if doNonJetSystematicsOnly:
        HmumuBtaggingEffCorrectorFixedCutBEff_77["m_systName"] = "All"
        HmumuBtaggingEffCorrectorFixedCutBEff_77["m_systVal"] = 1.0
        HmumuBtaggingEffCorrectorFixedCutBEff_77["m_outputSystName"] = "SystListBJet"
    c.setalg("BJetEfficiencyCorrector", HmumuBtaggingEffCorrectorFixedCutBEff_77)

    HmumuBtaggingEffCorrectorFixedCutBEff_85 = {"m_name": "HmumuBtaggingEffCorrectorFixedCutBEff_85",
                                "m_inContainerName": "PreORJets",
                                "m_operatingPt": "FixedCutBEff_85",
                                "m_taggerName": "MV2c10",
                                "m_corrFileName": BTaggingCorrFile,
                                "m_msgLevel":msgLevel,
                                }

    JetSystListBJet = ""
    if doJetSystematicsOnly:
        HmumuBtaggingEffCorrectorFixedCutBEff_85["m_inputAlgo"] = HmumuForwardJetSelection["m_outputAlgoSystNames"]
    if doNonJetSystematicsOnly:
        HmumuBtaggingEffCorrectorFixedCutBEff_85["m_systName"] = "All"
        HmumuBtaggingEffCorrectorFixedCutBEff_85["m_systVal"] = 1.0
        HmumuBtaggingEffCorrectorFixedCutBEff_85["m_outputSystName"] = "SystListBJet"
    c.setalg("BJetEfficiencyCorrector", HmumuBtaggingEffCorrectorFixedCutBEff_85)

    HmumuBtaggingEffCorrectorContinuous = {"m_name": "HmumuBtaggingEffCorrectorContinuous",
                                "m_inContainerName": "PreORJets",
                                "m_useContinuous":True,
                                "m_operatingPt": "Continuous",
                                "m_taggerName": "MV2c10",
                                "m_corrFileName": BTaggingCorrFile,
                                "m_msgLevel":msgLevel,
                                }
    JetSystListBJet = ""
    if doJetSystematicsOnly:
        HmumuBtaggingEffCorrectorContinuous["m_inputAlgo"] = HmumuForwardJetSelection["m_outputAlgoSystNames"]
    if doNonJetSystematicsOnly:
        HmumuBtaggingEffCorrectorContinuous["m_systName"] = "All"
        HmumuBtaggingEffCorrectorContinuous["m_systVal"] = 1.0
        HmumuBtaggingEffCorrectorContinuous["m_outputSystName"] = "SystListBJet"
        JetSystListBJet = HmumuBtaggingEffCorrectorContinuous["m_outputSystName"] + "_" + HmumuBtaggingEffCorrectorContinuous["m_taggerName"] + "_" + HmumuBtaggingEffCorrectorContinuous["m_operatingPt"]
    c.setalg("BJetEfficiencyCorrector", HmumuBtaggingEffCorrectorContinuous)

    HmumuMuonEfficiencyCorrector = {"m_name": "HmumuMuonEfficiencyCorrector",
                                "m_inContainerName": "PreORMuons",
                                "m_WorkingPointReco": MuonQual,
                                "m_WorkingPointIso":MuonIsoWP,
                                "m_MuTrigLegs":triggerSF,
                                "m_AllowZeroSF":True,
                                "m_msgLevel":msgLevel,
                                }

    MuonSystListReco = ""
    MuonSystListIso = ""
    MuonSystListTrig = ""
    if doNonJetSystematicsOnly:
        HmumuMuonEfficiencyCorrector["m_inputSystNamesMuons"] = HmumuMuonSelection["m_outputAlgoSystNames"]
        HmumuMuonEfficiencyCorrector["m_outputSystNamesReco"] = "MuonSystListReco"
        HmumuMuonEfficiencyCorrector["m_systValReco"] = 1.0
        HmumuMuonEfficiencyCorrector["m_systNameReco"] = "All"
        HmumuMuonEfficiencyCorrector["m_outputSystNamesIso"] = "MuonSystListIso"
        HmumuMuonEfficiencyCorrector["m_systValIso"] = 1.0
        HmumuMuonEfficiencyCorrector["m_systNameIso"] = "All"
        HmumuMuonEfficiencyCorrector["m_outputSystNamesTrig"] = "MuonSystListTrig"
        HmumuMuonEfficiencyCorrector["m_systValTrig"] = 1.0
        HmumuMuonEfficiencyCorrector["m_systNameTrig"] = "All"
        HmumuMuonEfficiencyCorrector["m_systName"] = "All"
        MuonSystListReco = HmumuMuonEfficiencyCorrector["m_outputSystNamesReco"] + "_Reco"  + HmumuMuonEfficiencyCorrector["m_WorkingPointReco"]
        MuonSystListIso = HmumuMuonEfficiencyCorrector["m_outputSystNamesIso"] + "_Iso"  + HmumuMuonEfficiencyCorrector["m_WorkingPointIso"]
        MuonSystListTrig = HmumuMuonEfficiencyCorrector["m_outputSystNamesTrig"] + "_" + triggerSF.split(",")[0] + "_Reco"  + HmumuMuonEfficiencyCorrector["m_WorkingPointReco"]

    c.setalg("MuonEfficiencyCorrector", HmumuMuonEfficiencyCorrector)

    HmumuElectronEfficiencyCorrector = {"m_name": "HmumuElectronEfficiencyCorrector",
                                    "m_inContainerName": "PreORElectrons",
                                    "m_WorkingPointPID": ElectronQual,
                                    "m_WorkingPointIso": ElectronIsoWP,
                                    "m_WorkingPointReco": ElectronQual,
                                    "m_msgLevel":msgLevel,
                                    }
    ElectronSystListPID = ""
    if doNonJetSystematicsOnly:
        HmumuElectronEfficiencyCorrector["m_outputSystNamesPID"] = "ElectronSystListPID"
        HmumuElectronEfficiencyCorrector["m_systValPID"] = 1.0
        HmumuElectronEfficiencyCorrector["m_systNamePID"] = "All"
        HmumuElectronEfficiencyCorrector["m_outputSystNamesIso"] = "ElectronSystListIso"
        HmumuElectronEfficiencyCorrector["m_systValIso"] = 1.0
        HmumuElectronEfficiencyCorrector["m_systNameIso"] = "All"
        HmumuElectronEfficiencyCorrector["m_outputSystNamesReco"] = "ElectronSystListReco"
        HmumuElectronEfficiencyCorrector["m_systValReco"] = 1.0
        HmumuElectronEfficiencyCorrector["m_inputSystNamesElectrons"] = HmumuElectronSelection["m_outputAlgoSystNames"]
        HmumuElectronEfficiencyCorrector["m_systNameReco"] = "All"
        HmumuElectronEfficiencyCorrector["m_correlationModel"] = "SIMPLIFIED"
        ElectronSystListPID = HmumuElectronEfficiencyCorrector["m_outputSystNamesPID"] + "_" + ElectronQual
        ElectronSystListIso = HmumuElectronEfficiencyCorrector["m_outputSystNamesIso"] + "_" + ElectronQual + "_isol" + ElectronIsoWP
        ElectronSystListReco = HmumuElectronEfficiencyCorrector["m_outputSystNamesReco"] + "_" + ElectronQual
    c.setalg("ElectronEfficiencyCorrector",HmumuElectronEfficiencyCorrector)

    HmumuMETConstructor      = {"m_name": "HmumuMETConstructor",
                               "m_referenceMETContainer":"MET_Reference_AntiKt4EMTopo",
                               "m_mapName":"METAssoc_AntiKt4EMTopo",
                               "m_coreName":"MET_Core_AntiKt4EMTopo",
                               "m_outputContainer":"MetContainer",
                               "m_inputJets":"PreORJets",
                               "m_inputElectrons":"PreORElectrons",
                               "m_inputMuons":"PreORMuons",
                               "m_msgLevel":msgLevel,
                               }
    if doJetSystematicsOnly:
        HmumuMETConstructor["m_jetSystematics"] = HmumuForwardJetSelection["m_outputAlgoSystNames"]
        HmumuMETConstructor["m_outputAlgoSystNames"] = HmumuMETConstructor["m_name"] + "_syst"
        HmumuMETConstructor["m_systName"] = "All"
        HmumuMETConstructor["m_runNominal"] = False
    if doNonJetSystematicsOnly:
        HmumuMETConstructor["m_systName"] = "All"
        HmumuMETConstructor["m_runNominal"] = False
        HmumuMETConstructor["m_eleSystematics"] = HmumuElectronSelection["m_outputAlgoSystNames"]
        HmumuMETConstructor["m_muonSystematics"] =  HmumuMuonSelection["m_outputAlgoSystNames"]
        HmumuMETConstructor["m_outputAlgoSystNames"] = HmumuMETConstructor["m_name"] + "_syst"

    c.setalg("METConstructor", HmumuMETConstructor)

    HmumuOverlapRemover =       {"m_name": "HmumuOverlapRemover",
                                "m_createSelectedContainers": True,
                                "m_decorateSelectedObjects":True,
                                "m_inContainerName_Muons":"PreORMuons",
                                "m_outContainerName_Muons":"HmumuMuons",
                                "m_inContainerName_Electrons":"PreORElectrons",
                                "m_outContainerName_Electrons":"HmumuElectrons",
                                "m_inContainerName_Jets":"PreORJets",
                                "m_outContainerName_Jets":"HmumuJets",
                                "m_applyRelPt": True,
                                "m_msgLevel":msgLevel,
                                }
    if doNonJetSystematicsOnly:
        HmumuOverlapRemover["m_outputAlgoSystNames"] = HmumuOverlapRemover["m_name"] + "_syst"
        HmumuOverlapRemover["m_inputAlgoMuons"] = HmumuMuonSelection["m_outputAlgoSystNames"]
        HmumuOverlapRemover["m_inputAlgoElectrons"] = HmumuElectronSelection["m_outputAlgoSystNames"]
    if doJetSystematicsOnly:
        HmumuOverlapRemover["m_outputAlgoSystNames"] = HmumuOverlapRemover["m_name"] + "_syst"
        HmumuOverlapRemover["m_inputAlgoJets"] = HmumuForwardJetSelection["m_outputAlgoSystNames"]
    c.setalg("HmumuOverlapRemover", HmumuOverlapRemover)

    ElectronQual += "LLH"
    mu_detailStr = "MuonIsoWP:" + MuonIsoWP + " " + "MuonQualWP:" + MuonQual + " "
    METDetailStr = "metTrk"
    if doNonJetSystematicsOnly: mu_detailStr = mu_detailStr + "SystListReco:" + MuonSystListReco + " " + "SystListIso:" + MuonSystListIso + " " + "SystListTrig:" + MuonSystListTrig
    el_detailStr = "ElectronIsoWP:" + ElectronIsoWP + " " + "ElectronQualWP:" + ElectronQual + " "
    if doNonJetSystematicsOnly: 
        el_detailStr = el_detailStr + "SystListPID:" + ElectronSystListPID + " " + "SystListIso:" + ElectronSystListIso + " " + "SystListReco:" + ElectronSystListReco
    jet_detailStr = "JetJVTWP:" + JetJVTWP + " " + "JetfJVTWP:" + JetfJVTWP
    if doNonJetSystematicsOnly: jet_detailStr = jet_detailStr + "SystListBJet:" + JetSystListBJet + " " + "SystListJVT:" + JetSystListJVT + " " + "SystListfJVT:" + JetSystListfJVT
    if doTruthLevel:
       jet_detailStr += " truth"

    c.setalg("HmumuTruthSelector", {"m_name": "HmumuTruthSelector",\
                                    "m_pT_min":-999999.0,\
                                    "m_createSelectedContainer":True,\
                                    "m_decorateSelectedObjects":False,\
                                    "m_inContainerName":"TruthParticles",\
                                    "m_outContainerName":"SkimmedTruthParticleContainer",\
                                    "m_msgLevel":msgLevel,\
                                    })

    HmumuTreeAlgoDict =   {"m_name": "HmumuTreeAlgo",\
                          "m_evtDetailStr":"pileup pileupsys",\
                          "m_elContainerName": "HmumuElectrons",\
                          "m_elDetailStr": el_detailStr,\
                          "m_muContainerName": "HmumuMuons",\
                          "m_muDetailStr": mu_detailStr,\
                          "m_jetContainerName": "HmumuJets",\
                          "m_jetDetailStr": jet_detailStr,\
                          "m_METContainerName":"MetContainer",\
                          "m_METDetailStr": METDetailStr,\
                          "m_applyTwoMuonsSelection" : applyTwoMuonsSelection,\
                          "m_applyLeadingMuonPtSelection" : applyLeadingMuonPtSelection,\
                          "m_minPtCut" : minPtCut,\
                          "m_applyTwoLeptonsOppositeChargeSelection" : applyTwoLeptonsOppositeChargeSelection,\
                          "m_applyTwoMuonsOppositeChargeSelection" : applyTwoMuonsOppositeChargeSelection,\
                          "m_applyZJetsRegionSelection" : applyZJetsRegionSelection,\
                          "m_applySignalRegionSelection" : applySignalRegionSelection,\
                          "m_applyOneBTagSelection": applyOneBTagSelection,\
                          "m_msgLevel":msgLevel,\
                          }

    if doTruthLevel:
       HmumuTreeAlgoDict["m_truthParticlesContainerName"] = "SkimmedTruthParticleContainer"
       HmumuTreeAlgoDict["m_truthParticlesDetailStr"] = "children parents"

    if doNonJetSystematicsOnly:
       HmumuTreeAlgoDict["m_muSystsVec"] =  HmumuOverlapRemover["m_outputAlgoSystNames"]
       HmumuTreeAlgoDict["m_metSystsVec"] = HmumuMETConstructor["m_outputAlgoSystNames"]
       HmumuTreeAlgoDict["m_elSystsVec"] = HmumuOverlapRemover["m_outputAlgoSystNames"]
    if doJetSystematicsOnly:
       HmumuTreeAlgoDict["m_jetSystsVec"] = HmumuOverlapRemover["m_outputAlgoSystNames"]
    if doJetSystematicsOnly:
        HmumuTreeAlgoDict["m_skipNominal"] = True

    c.setalg("HmumuTreeAlgo", HmumuTreeAlgoDict)
