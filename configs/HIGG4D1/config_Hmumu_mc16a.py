#R21 Hmumu Analysiw
from sys import path as sys_path
from os import path as os_path
WorkDir_DIR = os_path.expandvars('$WorkDir_DIR')
sys_path.insert(1, WorkDir_DIR + "/bin/HIGG4D1")

from xAH_config import xAH_config
from generalConfig import mc16aConfig, generalConfig
from config_Hmumu import setupHmumuAnalysis
c = xAH_config()

generalConfig["msgLevel"] = "info"
generalConfig["doSystematics"] = True

setupHmumuAnalysis(c, generalConfig, mc16aConfig)
