#R21 Hmumu Analysiw
from sys import path as sys_path
from os import path as os_path
WorkDir_DIR = os_path.expandvars('$WorkDir_DIR')
sys_path.insert(1, WorkDir_DIR + "/bin")

from xAH_config import xAH_config
from config_Hmumu import setupHmumuAnalysis
from generalConfig import mc16dConfig, generalConfig
c = xAH_config()

##change the configs to check the number of events passing each selection
generalConfig["applyOneMuonSelection"] = True
generalConfig["applyLeadingMuonPtSelection"] = True
generalConfig["minPtCut"] = 27e3
generalConfig["applyTwoMuonsSelection"] = True
generalConfig["applyTwoLeptonsOppositeChargeSelection"] = False
generalConfig["applyTwoMuonsOppositeChargeSelection"] = True
generalConfig["doJVT"] = False #Special change to run over AODs. For some reason they don't have truth jets

setupHmumuAnalysis(c,generalConfig, mc16dConfig)
