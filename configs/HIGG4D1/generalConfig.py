
generalConfig = {\
        "BTaggingCorrFile":"xAODBTaggingEfficiency/13TeV/2017-21-13TeV-MC16-CDI-2018-06-29_v1.root",\
        "MuonIsoWP":"FixedCutPflowLoose",\
        "ElectronIsoWP":"FCLoose",\
        "MuonQual":"Loose",\
        "ElectronQual":"Medium",\
        "MuonPt":15000,\
        "MuonEta":2.7,\
        "Muon_d0sig_max":3,\
        "Muon_z0sintheta_max":0.5,\
        "ElectronPt":7000,\
        "ElectronEta":2.47,\
        "Electron_d0sig_max":5,\
        "Electron_z0sintheta_max":0.5,\
        "JetJVTWP":"Medium",\
        "doJVT" : True,\
        "ElectronEffFolder":"ElectronEfficiencyCorrection/2015_2017/rel21.2/Moriond_February2018_v1/",\
        "applyLeadingMuonPtSelection": True,\
        "minPtCut" : 27000.0,\
        "applyTwoMuonsSelection": True,\
        "applyTwoLeptonsOppositeChargeSelection": True,\
        "applyTwoMuonsOppositeChargeSelection": True,\
        "applySignalRegionSelection": False,\
        "applyZJetsRegionSelection": False,\
        "applyOneBTagSelection": True,\
        "JetfJVTWP":"Medium",\
        "dofJVT":True,\
        "msgLevel" : "info", \
        "doTruthLevel" : True,\
        "doSystematics" : False,\
        "doJetSystematicsOnly" :False,\
        "doNonJetSystematicsOnly" :False,\
        "triggerSelection2015":"HLT_mu50,HLT_mu20_iloose_L1MU15",\
        "triggerSelection201620172018":"HLT_mu50,HLT_mu26_ivarmedium",\
        }

mc16aConfig = {\
        "triggers" : "HLT_mu50,HLT_mu26_ivarmedium,HLT_mu20_iloose_L1MU15",\
        "triggerSF" : "HLT_mu26_ivarmedium_OR_HLT_mu50,HLT_mu20_iloose_L1MU15_OR_HLT_mu50",\
        "GRL" : "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data15_13TeV/20170619/data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml,/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data16_13TeV/20180129/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml",\
        "LumiCalcFile" : "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root,/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root",\
        #"PRW_Files" : "$WorkDir_DIR/data/HmumuAnahelpers/samples/HIGG4D1/prw/mc16a_prw.root",\
        "PRW_Files" : "$WorkDir_DIR/data/HmumuAnahelpers/samples/HIGG1D2/prw_files/HIGG1D2_mc16a_prw.root",\
        #"PRW_Files" : "$WorkDir_DIR/data/HmumuAnahelpers/samples/HIGG8D1/prwfiles_mc16a/mc16a_prw.root.root",\
        }

mc16dConfig = {\
        "triggers" : "HLT_mu50,HLT_mu26_ivarmedium",\
        "triggerSF" : "HLT_mu26_ivarmedium_OR_HLT_mu50",\
        "GRL" : "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data17_13TeV/20180619/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml",\
        "LumiCalcFile" : "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root",\
        #"PRW_Files" : "$WorkDir_DIR/data/HmumuAnahelpers/samples/HIGG4D1/prw/mc16d_prw.root",\
        "PRW_Files" : "$WorkDir_DIR/data/HmumuAnahelpers/samples/HIGG1D2/prw_files/HIGG1D2_mc16d_prw.root",\
        #"PRW_Files" : "$WorkDir_DIR/data/HmumuAnahelpers/samples/HIGG8D1/prwfiles_mc16d/mc16d_prw.root.root",\
        }

mc16eConfig = {\
        "triggers" : "HLT_mu50,HLT_mu26_ivarmedium",\
        "triggerSF" : "HLT_mu26_ivarmedium_OR_HLT_mu50",\
        "GRL" : "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data18_13TeV/20190318/data18_13TeV.periodAllYear_DetStatus-v102-pro22-04_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml",\
        "LumiCalcFile" : "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data18_13TeV/20190318/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root",\
        #"PRW_Files" : "$WorkDir_DIR/data/HmumuAnahelpers/samples/HIGG4D1/prw/mc16d_prw.root",\
        "PRW_Files" : "$WorkDir_DIR/data/HmumuAnahelpers/samples/HIGG1D2/prw_files/HIGG1D2_mc16e_prw.root",\
        #"PRW_Files" : "$WorkDir_DIR/data/HmumuAnahelpers/samples/HIGG8D1/prwfiles_mc16d/mc16d_prw.root.root",\
        }
