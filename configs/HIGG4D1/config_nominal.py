#R21 Hmumu Analysiw
from sys import path as sys_path
from os import path as os_path
WorkDir_DIR = os_path.expandvars('$WorkDir_DIR')
sys_path.insert(1, WorkDir_DIR + "/bin/HIGG4D1")

from xAH_config import xAH_config
from generalConfig import mc16aConfig, generalConfig
from config_Hmumu import setupHmumuAnalysis
c = xAH_config()

#pass these with  xAH_run.py --extraOptions="(...)"
import argparse,shlex
parser = argparse.ArgumentParser()
parser.add_argument("--isData", action="store_true",  default=False, required=False, dest="isData")
parser.add_argument("--campaign", type=str, default="", required=True, dest="campaign")
args = parser.parse_args(shlex.split(args.extra_options))

if args.isData:
    generalConfig["doTruthLevel"]=False
else:
    generalConfig["doTruthLevel"]=True

if args.campaign == "mc16a":
    from generalConfig import mc16aConfig as config
if args.campaign == "mc16d":
    from generalConfig import mc16dConfig as config
if args.campaign == "mc16e":
    from generalConfig import mc16eConfig as config

generalConfig["msgLevel"] = "info"
generalConfig["doSystematics"] = False
generalConfig["doJetSystematicsOnly"] = False
generalConfig["doNonJetSystematicsOnly"] = False

setupHmumuAnalysis(c, generalConfig, config)
