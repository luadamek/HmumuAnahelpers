#R21 Hmumu Analysiw
from sys import path as sys_path
from os import path as os_path
WorkDir_DIR = os_path.expandvars('$WorkDir_DIR')
sys_path.insert(1, WorkDir_DIR + "/bin")

from xAH_config import xAH_config
#from generalConfig import generalConfig


#        "triggers" : "HLT_mu50,HLT_mu26_ivarmedium,HLT_mu20_iloose_L1MU15",\
#        "triggerSF" : "HLT_mu26_ivarmedium_OR_HLT_mu50,HLT_mu20_iloose_L1MU15_OR_HLT_mu50",\
#        "GRL" : "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data15_13TeV/20170619/data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml,/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data16_13TeV/20180129/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml",\
#        "LumiCalcFile" : "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root,/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root",\
#        "PRW_Files" : "$WorkDir_DIR/data/HmumuAnahelpers/samples/v01/prw/mc16a_prw.root",\

#Set up the needed GRL and PRW files for mc16a
LumiCalcFile = "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root,/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root"
PRW_Files = "$WorkDir_DIR/data/HmumuAnahelpers/samples/HIGG4D1/prw/mc16a_prw.root"
GRL = "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data15_13TeV/20170619/data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml,/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data16_13TeV/20180129/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml"
msgLevel = "debug"

c = xAH_config()


''' Set up all the algorithms '''
BasicEventSelection =            {"m_name": "BasicEventSelection",
                                 "m_applyGRLCut": False,
                                 "m_lumiCalcFileNames": LumiCalcFile,
                                 "m_PRWFileNames": PRW_Files,
                                 "m_doPUreweighting": True,
                                 "m_applyPrimaryVertexCut": False,
                                 "m_applyEventCleaningCut": False,
                                 "m_applyCoreFlagsCut": False,
                                 "m_applyTriggerCut": False,
                                 "m_useMetaData": True,
                                 "m_cutFlowStreamName":"EventPreselection",
                                 "m_checkDuplicatesData": True,
                                 "m_checkDuplicatesMC": True}
c.setalg("BasicEventSelection", BasicEventSelection)

'''set up muon calibration'''
HmumuMuonCalibration =       {"m_name": "HmumuMuonCalibrator",
                            "m_inContainerName": "Muons",
                            "m_outContainerName": "CalibMuons",
                            "m_sort": True,
                            "m_msgLevel":msgLevel,
                            }
c.setalg("MuonCalibrator", HmumuMuonCalibration)

'''set up jet calibration'''
HmumuJetCalibration =      {"m_name": "HmumuJetCalibrator",
                            "m_inContainerName": "AntiKt4EMTopoJets",
                            "m_jetAlgo": "AntiKt4EMTopo",
                            "m_outContainerName": "CalibJets",
                            "m_calibConfigData":"JES_data2017_2016_2015_Recommendation_Aug2018_rel21.config",
                            "m_calibConfig":"JES_data2017_2016_2015_Recommendation_Aug2018_rel21.config",
                            "m_calibConfigFullSim": "JES_data2017_2016_2015_Recommendation_Aug2018_rel21.config",
                            "m_calibSequence": "JetArea_Residual_EtaJES_GSC",
                            "m_calibArea": "00-04-81",
                            "m_JESCalibArea":"CalibArea-05",
                            "m_sort": True,
                            "m_doCleaning": False,
                            "m_forceInsitu": True,
                            "m_msgLevel":msgLevel,
                            }
c.setalg("JetCalibrator", HmumuJetCalibration)


'''HmumuMuonSelection and Jet Selection'''
HmumuMuonPreSelection = {"m_name": "HmumuMuonPreSelector",
                      "m_inContainerName": "CalibMuons",
                      "m_outContainerName": "PreSelectedMuons",
                      "m_pT_min": 7e3,
                      "m_eta_max": 2.7,
                      "m_muonQualityStr":"Loose",
                      "m_MinIsoWPCut": "LooseTrackOnly",
                      "m_useCutFlow":True,
                      "m_isUsedBefore":False,
                      "m_createSelectedContainer":True,
                      "m_msgLevel":msgLevel,
                      }
c.setalg("MuonSelector", HmumuMuonPreSelection)


HmumuJetPreSelection =        {"m_name": "HmumuJetSelector",
                            "m_pT_max": 25e3,
                            "m_eta_max": 4.5,
                            "m_doJVT": True,
                            "m_JVTCut":0.59,
                            "m_cleanJets":True,
                            "m_useCutFlow":True,
                            "m_createSelectedContainer":True,
                            "m_inContainerName":"CalibJets",
                            "m_outContainerName":"PreselectedJets",
                            }
c.setalg("JetSelector", HmumuJetPreSelection)

