#R21 Hmumu Analysiw

from xAH_config import xAH_config
c = xAH_config()

triggers = "HLT_mu50,HLT_mu26_ivarmedium,HLT_mu20_iloose_L1MU15"
triggerSF = "HLT_mu26_ivarmedium_OR_HLT_mu50,HLT_mu20_iloose_L1MU15_OR_HLT_mu50"

#2017 and mc16d configuration of everything
GRL = "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data17_13TeV/20180619/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml"

LumiCalcFile = "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root"

#I NEED TO CREATE ONE OF THESE PRW FILES FOR ALL OF THE SAMPLES THAT WE ARE USING
PRW_Files = "$WorkDir_DIR/data/HmumuAnahelpers/samples/v01/prw/mc16d_prw/mc16d_prw.root"


BTaggingCorrFile = "xAODBTaggingEfficiency/13TeV/2017-21-13TeV-MC16-CDI-2018-06-29_v1.root"

MuonIsoWP = "LooseTrackOnly"
ElectronIsoWP = "Loose"
MuonQual = "Loose"
ElectronQual = "Medium"

MuonPt = 15e3
MuonEta = 2.5
Muon_d0sig_max = 3
Muon_z0sintheta_max = 0.5 

ElectronPt = 7e3
ElectronEta = 2.47
Electron_d0sig_max = 5
Electron_z0sintheta_max = 0.5 

JetJVTWP = "Medium"

Directory = "ElectronEfficiencyCorrection/2015_2017/rel21.2/Moriond_February2018_v1/"
elecRecoFile = Directory + "offline/efficiencySF.offline.RecoTrk.root"
elecIdentificationFile = Directory + "offline/efficiencySF.offline." + ElectronQual + "LLH_d0z0_v13.root"
elecIsolationFile = Directory + "isolation/efficiencySF.Isolation." + ElectronQual + "LLH_d0z0_v13_isol"+ ElectronIsoWP + ".root"


''' Set up all the algorithms '''
c.setalg("BasicEventSelection", {"m_name": "BasicEventSelection",
                                 "m_applyGRLCut": True,
                                 "m_GRLxml":GRL,
                                 "m_lumiCalcFileNames": LumiCalcFile,
                                 "m_PRWFileNames": PRW_Files,
                                 "m_doPUreweighting": True,
                                 "m_doPUreweightingSys": True,
                                 "m_applyPrimaryVertexCut": True,
                                 "m_applyEventCleaningCut": True,
                                 "m_applyCoreFlagsCut": False,
                                 "m_applyTriggerCut": True,
                                 "m_triggerSelection": triggers,
                                 "m_PVNTrack": 2,
                                 "m_useMetaData": True,
                                 "m_checkDuplicatesData": False,
                                 "m_checkDuplicatesMC": False})

c.setalg("MuonCalibrator", {"m_name": "HmumuMuonCalibrator",
                            "m_inContainerName": "Muons",
                            "m_outContainerName": "CalibMuons",
                            "m_sort": True,
                            "m_msgLevel":"debug",
                            })


c.setalg("JetCalibrator", {"m_name": "HmumuJetCalibrator",
                            "m_inContainerName": "AntiKt4EMTopoJets",
                            "m_jetAlgo": "AntiKt4EMTopo",
                            "m_outContainerName": "CalibJets",
                            "m_calibConfigData":"JES_data2017_2016_2015_Recommendation_Aug2018_rel21.config",
                            "m_calibConfig":"JES_data2017_2016_2015_Recommendation_Aug2018_rel21.config",
                            "m_calibConfigFullSim": "JES_data2017_2016_2015_Recommendation_Aug2018_rel21.config",
                            "m_calibSequence": "JetArea_Residual_EtaJES_GSC",
                            "m_calibArea": "00-04-81",
                            "m_sort": True,
                            "m_doCleaning": True,
                            "m_forceInsitu": True,
                            #"m_forceSmearing": True,
                            "m_msgLevel":"debug",
                            })

c.setalg("ElectronCalibrator", {"m_name": "HmumuElectronCalibrator",
                             "m_inContainerName": "Electrons",
                             "m_outContainerName": "CalibElectrons",
                             "m_esModel": "es2017_R21_v0",
                             "m_sort": True,
                             "m_msgLevel":"debug",
                             })

c.setalg("MuonSelector",    {"m_name": "HmumuMuonSelection",
                            "m_inContainerName": "CalibMuons",
                            "m_createSelectedContainer": True,
                            "m_decorateSelectedObjects":False,
                            "m_pT_min": MuonPt,
                            "m_eta_max": MuonEta,
                            "m_d0sig_max": Muon_d0sig_max,
                            "m_z0sintheta_max": Muon_z0sintheta_max,
                            "m_muonQualityStr": MuonQual,
                            "m_MinIsoWPCut": MuonIsoWP,
                            "m_outContainerName" : "PreORMuons",
                            "m_singleMuTrigChains": triggers,
                            "m_msgLevel":"debug",
                            })

c.setalg("JetSelector",    {"m_name": "HmumuJetSelection",
                            "m_inContainerName": "CalibJets",
                            "m_pT_min": 25000,
                            "m_eta_max": 4.5,
                            "m_SFFileJVT": "JetJvtEfficiency/Moriond2018/JvtSFFile_EMTopoJets.root",
                            "m_doJVT": True,
                            "m_WorkingPointJVT": JetJVTWP,
                            "m_outContainerName": "NoForwardSelectionJets",
                            "m_createSelectedContainer": True,
                            "m_decorateSelectedObjects": False,
                            "m_msgLevel":"debug",
                            })

c.setalg("HmumuForwardJetSelection", {"m_name": "HmumuForwardJetSelectionAlgo",
                             "m_inContainerName": "NoForwardSelectionJets",
                             "m_eta_min": 2.4,
                             "m_eta_max": 4.5,
                             "m_pt_min": 30e3,
                             "m_outContainerName": "PreORJets",
                             "m_msgLevel":"debug",
                             })

c.setalg("ElectronSelector",{"m_name": "HmumuElectronSelection",
                            "m_inContainerName": "CalibElectrons",
                            "m_createSelectedContainer": True,
                            "m_decorateSelectedObjects":False,
                            "m_d0sig_max": Electron_d0sig_max,
                            "m_z0sintheta_max": Electron_z0sintheta_max,
                            "m_pT_min": ElectronPt,
                            "m_eta_max": ElectronEta,
                            "m_doLHPID": True,
                            "m_doLHPIDcut": True,
                            "m_LHOperatingPoint": ElectronQual,
                            "m_MinIsoWPCut": ElectronIsoWP,
                            "m_vetoCrack": True,
                            "m_readIDFlagsFromDerivation": True,
                            "m_outContainerName" : "PreORElectrons",
                            "m_msgLevel":"debug",
                            })

c.setalg("BJetEfficiencyCorrector", {"m_name": "HmumuBtaggingEffCorrector60",
                            "m_inContainerName": "PreORJets",
                            "m_operatingPt": "FixedCutBEff_60",
                            "m_taggerName":"MV2c10",
                            "m_corrFileName": BTaggingCorrFile,
                            "m_msgLevel":"debug",
                            })

c.setalg("BJetEfficiencyCorrector", {"m_name": "HmumuBtaggingEffCorrector70",
                            "m_inContainerName": "PreORJets",
                            "m_operatingPt": "FixedCutBEff_70",
                            "m_taggerName":"MV2c10",
                            "m_corrFileName": BTaggingCorrFile,
                            "m_msgLevel":"debug",
                            })
c.setalg("BJetEfficiencyCorrector", {"m_name": "HmumuBtaggingEffCorrector77",
                            "m_inContainerName": "PreORJets",
                            "m_operatingPt": "FixedCutBEff_77",
                            "m_taggerName":"MV2c10",
                            "m_corrFileName": BTaggingCorrFile,
                            "m_msgLevel":"debug",
                            })
c.setalg("BJetEfficiencyCorrector", {"m_name": "HmumuBtaggingEffCorrector85",
                            "m_inContainerName": "PreORJets",
                            "m_operatingPt": "FixedCutBEff_85",
                            "m_taggerName":"MV2c10",
                            "m_corrFileName": BTaggingCorrFile,
                            "m_msgLevel":"debug",
                            })

c.setalg("MuonEfficiencyCorrector", {"m_name": "HmumuMuonEfficiencyCorrector",
                            "m_inContainerName": "PreORMuons",
                            "m_WorkingPointReco": MuonQual,
                            "m_WorkingPointIso":MuonIsoWP,
                            "m_MuTrigLegs":triggerSF,
                            "m_calibRelease":"180312_TriggerUpdate",
                            "m_AllowZeroSF":True,
                            "m_msgLevel":"debug",
                            })

c.setalg("ElectronEfficiencyCorrector", {"m_name": "HmumuElectronEfficiencyCorrector",
                            "m_inContainerName": "PreORElectrons",
                            "m_corrFileNamePID":elecIdentificationFile,
                            "m_corrFileNameIso":elecIsolationFile,
                            "m_corrFileNameReco":elecRecoFile,
                            "m_msgLevel":"debug",
                            })

c.setalg("METConstructor", {"m_name": "HmumuMETConstructor",
                           "m_referenceMETContainer":"MET_Reference_AntiKt4EMTopo",
                           "m_mapName":"METAssoc_AntiKt4EMTopo",
                           "m_coreName":"MET_Core_AntiKt4EMTopo",
                           "m_outputContainer":"MetContainer",
                           "m_inputJets":"PreORJets",
                           "m_inputElectrons":"PreORElectrons",
                           "m_inputMuons":"PreORMuons",
                           "m_msgLevel":"debug",
                           })

c.setalg("HmumuOverlapRemover", {"m_name": "HmumuOverlapRemover",
                            "m_createSelectedContainers": True,
                            "m_decorateSelectedObjects":False,
                            "m_inContainerName_Muons":"PreORMuons",
                            "m_outContainerName_Muons":"HmumuMuons",
                            "m_inContainerName_Electrons":"PreORElectrons",
                            "m_outContainerName_Electrons":"HmumuElectrons",
                            "m_inContainerName_Jets":"PreORJets",
                            "m_outContainerName_Jets":"HmumuJets",
                            "m_applyRelPt": True,
                            "m_msgLevel":"debug",
                            })

ElectronQual += "LLH"
mu_detailStr = "MuonIsoWP:" + MuonIsoWP + " " + "MuonQualWP:" + MuonQual + " "
el_detailStr = "ElectronIsoWP:" + ElectronIsoWP + " " + "ElectronQualWP:" + ElectronQual + " "
jet_detailStr = "JetJVTWP:" + JetJVTWP + " " 
METDetailStr = "metTrk"

c.setalg("HmumuTreeAlgo", {"m_name": "HmumuTreeAlgo",
                      "m_evtDetailStr":"pileup",
                      "m_elContainerName": "HmumuElectrons",
                      "m_elDetailStr": el_detailStr,
                      "m_muContainerName": "HmumuMuons",
                      "m_muDetailStr": mu_detailStr,
                      "m_jetContainerName": "HmumuJets",
                      "m_jetDetailStr": jet_detailStr,
                      "m_METContainerName":"MetContainer",
                      "m_METDetailStr": METDetailStr,
                      "m_msgLevel":"debug",
                      })
