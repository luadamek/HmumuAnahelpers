/**
 * @file   HmumuEventSelection.h
 * @author Gabriel Facini <gabriel.facini@cern.ch>
 * @author Marco Milesi <marco.milesi@cern.ch>
 * @author Jeff Dandoy <jeff.dandoy@cern.ch>
 * @author John Alison <john.alison@cern.ch>
 * @brief  Algorithm performing general basic cuts for an analysis (GRL, Event Cleaning, Min nr. Tracks for PV candidate).
 *
 */

#ifndef HmumuAnahelpers_HmumuEventSelection_H
#define HmumuAnahelpers_HmumuEventSelection_H

// ROOT include(s):
#include "TH1D.h"

// algorithm wrapper
#include "xAODAnaHelpers/Algorithm.h"

// external tools include(s):
#include "AsgTools/AnaToolHandle.h"
#include "AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h"
#include "AsgAnalysisInterfaces/IPileupReweightingTool.h"
#include "TrigConfInterfaces/ITrigConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "PATInterfaces/IWeightTool.h"

/**
  @rst
    This algorithm performs the very basic event selection. This should be the first algo in the algo chain. It can create weighted and unweighted cutflow objects to be picked up downstream by other xAH algos, and your own. The selection applied in data only is:

      - GRL (can be turned off)
      - LAr Error
      - Tile Error
      - Core Flag

    .. note:: For MC only, the pileup reweight can also be applied.

    In both data and simulation (MC), the following cuts are applied

      - the highest sum :math:`p_{T}^2` primary vertex has 2 or more tracks (see :cpp:member:`~HmumuEventSelection::m_applyPrimaryVertexCut`)
      - trigger requirements (see :cpp:member:`~HmumuEventSelection::m_applyTriggerCut`)

    For derivations, the metadata can be accessed and added to the cutflow for normalization. The parameters to control the trigger are described in this header file. If one wants to write out some of the trigger information into a tree using :cpp:class:`~HelpTreeBase`, flags must be set here.

  @endrst
*/
class HmumuEventSelection : public xAH::Algorithm
{
  public:
  // Sample type settings
    /// @brief Protection when running on truth xAOD
    std::string m_triggerSelection2015;
    std::string m_triggerSelection201620172018;

  private:

    int m_eventCounter;     //!

    // cutflow
    TH1D* m_cutflowHist = nullptr;      //!
    TH1D* m_cutflowHistW = nullptr;     //!

    std::vector< std::string > triggerVec; //!
    std::vector< std::string > triggers2015Vec; //!
    std::vector< std::string > triggers201620172018Vec; //!

    asg::AnaToolHandle<CP::IPileupReweightingTool> m_pileup_tool_handle{"CP::PileupReweightingTool/Pileup"}; //!

  public:
    // Tree *myTree; //!
    // TH1 *myHist; //!
    //

    // this is a standard constructor
    HmumuEventSelection ();

    // these are the functions inherited from Algorithm
    virtual EL::StatusCode setupJob (EL::Job& job);
    virtual EL::StatusCode fileExecute ();
    virtual EL::StatusCode histInitialize ();
    virtual EL::StatusCode changeInput (bool firstFile);
    virtual EL::StatusCode initialize ();
    virtual EL::StatusCode execute ();
    virtual EL::StatusCode postExecute ();
    virtual EL::StatusCode finalize ();
    virtual EL::StatusCode histFinalize ();

    /// @cond
    // this is needed to distribute the algorithm to the workers
    ClassDef(HmumuEventSelection, 1);
    /// @endcond
};

#endif
