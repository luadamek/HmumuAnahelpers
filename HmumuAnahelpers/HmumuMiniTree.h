
#ifndef HmumuAnahelpers_HmumuMiniTree_H
#define HmumuAnahelpers_HmumuMiniTree_H
#include "HmumuAnahelpers/HmumuTreeBase.h"
#include "TTree.h"

/**
  @brief Define and fill the MJB TTrees.  Inherits from xAODAnaHelpers::HelpTreeBase
*/
class HmumuMiniTree : public HmumuTreeBase
{

    private:
  std::vector<float> m_selectedMuonsPos_pt;
  std::vector<float> m_selectedMuonsPos_e;
  std::vector<float> m_selectedMuonsPos_phi;
  std::vector<float> m_selectedMuonsPos_eta;
  std::vector<float> m_selectedMuonsPos_iso;
  std::vector<float> m_selectedMuonsPos_veto;
  std::vector<float> m_selectedMuonsNeg_pt;
  std::vector<float> m_selectedMuonsNeg_e;
  std::vector<float> m_selectedMuonsNeg_phi;
  std::vector<float> m_selectedMuonsNeg_eta;
  std::vector<float> m_selectedMuonsNeg_iso;
  std::vector<float> m_selectedMuonsNeg_veto;
  std::vector<int> m_selectedMuonsPos_TruthOrigin;
  std::vector<int> m_selectedMuonsNeg_TruthOrigin;
  std::vector<int> m_selectedElectronsPos_TruthOrigin;
  std::vector<int> m_selectedElectronsNeg_TruthOrigin;
  std::vector<int> m_selectedMuonsPos_TruthType;
  std::vector<int> m_selectedMuonsNeg_TruthType;
  std::vector<int> m_selectedElectronsPos_TruthType;
  std::vector<int> m_selectedElectronsNeg_TruthType;
  std::vector<float> m_selectedJets_pt;
  std::vector<float> m_selectedJets_e;
  std::vector<float> m_selectedJets_phi;
  std::vector<float> m_selectedJets_eta;
  std::vector<float> m_selectedJets_iso;
  std::vector<float> m_selectedJets_veto;
  //std::vector<int> m_selectedJets_TruthOrigin;
  //std::vector<int> m_selectedJets_TruthType;
  std::vector<float> m_selectedElectronsPos_pt;
  std::vector<float> m_selectedElectronsPos_e;
  std::vector<float> m_selectedElectronsPos_phi;
  std::vector<float> m_selectedElectronsPos_eta;
  std::vector<float> m_selectedElectronsPos_iso;
  std::vector<float> m_selectedElectronsPos_veto;
  std::vector<float> m_selectedElectronsNeg_pt;
  std::vector<float> m_selectedElectronsNeg_e;
  std::vector<float> m_selectedElectronsNeg_phi;
  std::vector<float> m_selectedElectronsNeg_eta;
  std::vector<float> m_selectedElectronsNeg_iso;
  std::vector<float> m_selectedElectronsNeg_veto;

  std::map<std::string, std::string>* m_jetWPs;
  std::map<std::string, std::string>* m_electronWPs;
  std::map<std::string, std::string>* m_muonWPs;

  int m_decayPdgId;

  float m_jetJVTSF = 1.0;
  std::vector<float> m_jetJVTSF_Syst;
  float m_jetfJVTSF = 1.0;
  std::vector<float> m_jetfJVTSF_Syst;
  float m_eventBTagSF60 = 1.0;
  std::vector<float> m_eventBTagSF60_Syst;
  float m_eventBTagSF70 = 1.0;
  std::vector<float> m_eventBTagSF70_Syst;
  float m_eventBTagSF77 = 1.0;
  std::vector<float> m_eventBTagSF77_Syst;
  float m_eventBTagSF85 = 1.0;
  std::vector<float> m_eventBTagSF85_Syst;
  float m_eventBTagSFContinuous = 1.0;
  std::vector<float> m_eventBTagSFContinuous_Syst;
  float m_eventBTagIneffSFContinuous = 1.0;
  std::vector<float> m_eventBTagIneffSFContinuous_Syst;

  std::vector<char> m_jetsBTagWP60;
  std::vector<char> m_jetsBTagWP70;
  std::vector<char> m_jetsBTagWP77;
  std::vector<char> m_jetsBTagWP85;
  std::vector<float> m_jetsBTagWeight;
  std::vector<int> m_jetsBTagQuantile;

  SG::AuxElement::ConstAccessor< std::map<std::string,char> >* m_acc_muonTriggerMatch;
  SG::AuxElement::ConstAccessor<char>* m_accbtag_WP60;
  SG::AuxElement::ConstAccessor< std::vector<float> >* m_accbtag_SF60;
  SG::AuxElement::ConstAccessor<char>* m_accbtag_WP70;
  SG::AuxElement::ConstAccessor< std::vector<float> >* m_accbtag_SF70;
  SG::AuxElement::ConstAccessor<char>* m_accbtag_WP77;
  SG::AuxElement::ConstAccessor< std::vector<float> >* m_accbtag_SF77;
  SG::AuxElement::ConstAccessor<char>* m_accbtag_WP85;
  SG::AuxElement::ConstAccessor< std::vector<float> >* m_accbtag_SF85;
  SG::AuxElement::ConstAccessor<float>* m_accbtag_weight;
  SG::AuxElement::ConstAccessor<int>* m_accbtag_quantile;
  SG::AuxElement::ConstAccessor< std::vector<float> >* m_accbtag_SFContinuous;
  SG::AuxElement::ConstAccessor< std::vector<float> >* m_accbtag_IneffSFContinuous;

  SG::AuxElement::ConstAccessor< std::vector<float> >* m_accjvt_SF;
  SG::AuxElement::ConstAccessor< std::vector<float> >* m_accfjvt_SF;

  std::vector<float> m_muonQualSF_Syst;
  std::vector<float> m_muonIsoSF_Syst;

  std::vector<float> m_muonTrigSF_HLT_mu26_ivarmedium_OR_HLT_mu50_Syst;
  std::vector<float> m_muonTrigSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_Syst;

  int m_TrigMatch_HLT_mu26_ivarmedium = 0;
  int m_TrigMatch_HLT_mu20_iloose_L1MU15 = 0;
  int m_TrigMatch_HLT_mu50 = 0;
  int m_TrigMatch_SubLeadingMuonBelowThreshold = 0;

  std::vector<float>  m_muonTrigHLT26SF;
  std::vector<std::vector<float> > m_muonTrigHLT26SF_Syst;
  std::vector<float>  m_muonTrigHLT26Eff;
  std::vector<std::vector<float> > m_muonTrigHLT26Eff_Syst;
  std::vector<float>  m_muonTrigHLT20SF;
  std::vector<std::vector<float> >  m_muonTrigHLT20SF_Syst;
  std::vector<float>  m_muonTrigHLT20Eff;
  std::vector<std::vector<float> >  m_muonTrigHLT20Eff_Syst;

  std::vector<float>  m_muonPt;

  std::vector<char> m_vec_TrigMatch_HLT_mu26_ivarmedium;
  std::vector<char> m_vec_TrigMatch_HLT_mu20_iloose_L1MU15;
  std::vector<char> m_vec_TrigMatch_HLT_mu50;

  std::string m_muonQualSFString;
  std::string m_muonIsoSFString;
  std::string m_muonTTVASFString; 

  std::string m_muonTriggerHLT20SFString;
  std::string m_muonTriggerHLT20EffString;
  std::string m_muonTriggerHLT26SFString;
  std::string m_muonTriggerHLT26EffString;

  std::string m_muonTrigMatchString;

  SG::AuxElement::ConstAccessor<std::vector<float> >* m_acc_muonQualSF;
  SG::AuxElement::ConstAccessor<std::vector<float> >* m_acc_muonIsoSF;
  SG::AuxElement::ConstAccessor<std::vector<float> >* m_acc_muonTTVASF;

  SG::AuxElement::ConstAccessor<std::vector<float> >* m_acc_muonTriggerHLT20SF;
  SG::AuxElement::ConstAccessor<std::vector<float> >* m_acc_muonTriggerHLT26SF;

  SG::AuxElement::ConstAccessor<std::vector<float> >* m_acc_muonTriggerHLT20Eff;
  SG::AuxElement::ConstAccessor<std::vector<float> >* m_acc_muonTriggerHLT26Eff;

  std::string m_electronPIDSFString;
  std::string m_electronIsoSFString;
  std::string m_electronQualSFString;

  SG::AuxElement::ConstAccessor<std::vector<float> >*  m_acc_electronPIDSF;
  SG::AuxElement::ConstAccessor<std::vector<float> >*  m_acc_electronIsoSF;
  SG::AuxElement::ConstAccessor<std::vector<float> >*  m_acc_electronQualSF;

  SG::AuxElement::ConstAccessor<float >* m_acc_electronPromptVeto;
  SG::AuxElement::ConstAccessor<float >* m_acc_electronPromptIso;
  SG::AuxElement::ConstAccessor<float >* m_acc_muonPromptIso;
  SG::AuxElement::ConstAccessor<float >* m_acc_muonPromptVeto;

  SG::AuxElement::ConstAccessor< int >* m_acc_electronTruthOrigin;
  SG::AuxElement::ConstAccessor< int >* m_acc_electronTruthType;
  SG::AuxElement::ConstAccessor< int >* m_acc_muonTruthOrigin;
  SG::AuxElement::ConstAccessor< int >* m_acc_muonTruthType;
  //SG::AuxElement::ConstAccessor< int >* m_acc_jetTruthOrigin;
  //SG::AuxElement::ConstAccessor< int >* m_acc_jetTruthType;

  std::vector<float> m_electronPIDSF_Syst;
  std::vector<float> m_electronIsoSF_Syst;
  std::vector<float> m_electronQualSF_Syst;
  
    public:
    /** @brief Create the base HelpTreeBase instance */
    HmumuMiniTree(xAOD::TEvent * event, TTree* tree, TFile* file, float units, bool debug, xAOD::TStore* store) ;
    /** @brief Standard destructor*/
    ~HmumuMiniTree();

    /** @brief Connect the branches for event-level variables */
    void AddEventUser( const std::string detailStr = "" );
    /** @brief Connect the branches for jet-level variables */
    void AddJetsUser( const std::string detailStr = "" , const std::string jetName = "jet");
    void AddMuonsUser( const std::string detailStr = "" , const std::string muName = "muon");
    void FinalizeMuonsUser();
    void AddElectronsUser( const std::string detailStr = "", const std::string elecName = "electron");
    /** @brief Fill the TTree with event-level variables */
    void FillEventUser( const xAOD::EventInfo* eventInfo );
    /** @brief Fill the TTree with jet-level variables */
    void FillJetsUser( const xAOD::Jet* jet, const std::string jetName = "jet");
    /** @brief Clear vectors used by jet-level variables*/
    void ClearJetsUser(const std::string jetName = "jet");
    
     void makeBranch(const TString & label, const TString & varname, int & var);
     void makeBranch(const TString & label, const TString & varname, char & var);
     void makeBranch(const TString & label, const TString & varname, float & var);
     void makeBranch(const TString & label, const TString & varname, double & var);
     void makeBranch(const TString & label, const TString & varname, std::vector<int> & var);
     void makeBranch(const TString & label, const TString & varname, std::vector<unsigned int> & var);
     void makeBranch(const TString & label, const TString & varname, std::vector<char> & var);
     void makeBranch(const TString & label, const TString & varname, std::vector<float> & var);
     void makeBranch(const TString & label, const TString & varname, std::vector<double> & var);

     void ClearElectronsUser(const std::string elecName);
     void ClearMuonsUser(const std::string muonName);

     void FillElectronsUser(const xAOD::Electron* el, const std::string detailStr);
     void FillMuonsUser(const xAOD::Muon* muon, const std::string detailStr);


};
#endif
