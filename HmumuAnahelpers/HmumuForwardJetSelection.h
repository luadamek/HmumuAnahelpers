// E/p analysis for run 2
// Joakim Olsson (joakim.olsson@cern.ch)

#ifndef HmumuAnahelpers_HmumuForwardJetSelection_H
#define HmumuAnahelpers_HmumuForwardJetSelection_H

// EDM include(s):
#include "xAODTracking/VertexContainer.h"
#include <xAODJet/JetContainer.h>

// algorithm wrapper
#include "xAODAnaHelpers/Algorithm.h"

// ROOT include(s):
#include "TH1D.h"

class HmumuForwardJetSelection : public xAH::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:

  // configuration variables
  std::string m_inContainerName = "";    // input container name
  std::string m_outContainerName = "";   // output container name
  std::string m_outputAlgoSystNames = "";   // output container systematic names
  std::string m_inputAlgoSystNames = "";

  float m_pt_min = 0.0;          // Minimum p_T of jets
  float m_eta_min = -1.0 * 1e8;
  float m_eta_max = 1e8;

private:
  //private varaibles would normally go here
  TH1D* m_jet_cutflowHist_1 =  nullptr;
  int m_fwd_selection_bin;

public:
  // Tree *myTree; //!
  // TH1 *myHist; //!

  // this is a standard constructor
  HmumuForwardJetSelection(std::string className = "HmumuForwardJetSelection");
  ~HmumuForwardJetSelection();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();
  void selectJets(ConstDataVector<xAOD::JetContainer>* inJets, ConstDataVector<xAOD::JetContainer>* outJets);

  // added functions not from Algorithm

  /// @cond
  // this is needed to distribute the algorithm to the workers
  ClassDef(HmumuForwardJetSelection, 1);
  /// @endcond
};

#endif
