#ifndef xAODAnaHelpers_HmumuTruthContainer_H
#define xAODAnaHelpers_HmumuTruthContainer_H

#include <TTree.h>
#include <TLorentzVector.h>

#include <vector>
#include <string>

#include "xAODTruth/TruthParticle.h"

#include <xAODAnaHelpers/HelperClasses.h>
#include <xAODAnaHelpers/HelperFunctions.h>

#include <xAODAnaHelpers/TruthPart.h>
#include <xAODAnaHelpers/ParticleContainer.h>

namespace xAH {

    class HmumuTruthContainer : public ParticleContainer<TruthPart,HelperClasses::TruthInfoSwitch>
    {
    public:
      HmumuTruthContainer(const std::string& name = "truth", const std::string& detailStr="", float units = 1e3);
      virtual ~HmumuTruthContainer();
    
      virtual void setTree    (TTree *tree);
      virtual void setBranches(TTree *tree);
      virtual void clear();
      virtual void FillTruth( const xAOD::TruthParticle* truth );
      virtual void FillTruth( const xAOD::IParticle* particle );
      using ParticleContainer::setTree; // make other overloaded version of execute() to show up in subclass

    protected:

      virtual void updateParticle(uint idx, TruthPart& truth);

    private:


      //
      // Vector branches
      //
      
      // All
      std::vector<int>* m_pdgId;
      std::vector<int>* m_status;
      std::vector<int>* m_barcode;

      // Kinematic variables to store
      std::vector<float>* m_Energy;
      std::vector<float>* m_Pt;
      std::vector<float>* m_Eta;
      std::vector<float>* m_Phi;
    
      // type
      std::vector<int>* m_is_higgs;
      std::vector<int>* m_is_bhad;

      // bVtx
      std::vector<float>* m_Bdecay_x;
      std::vector<float>* m_Bdecay_y;
      std::vector<float>* m_Bdecay_z;

      // parents
      std::vector<int>* m_nParents;
      std::vector< std::vector<int> >* m_parent_pdgId;
      std::vector< std::vector<int> >* m_parent_barcode;
      std::vector< std::vector<int> >* m_parent_status;

      // children
      std::vector<int>* m_nChildren;
      std::vector< std::vector<int> >* m_child_pdgId;
      std::vector< std::vector<int> >* m_child_barcode;
      std::vector< std::vector<int> >* m_child_status;

    };
}



#endif // xAODAnaHelpers_HmumuTruthContainer_H
