#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODEventInfo/EventInfo.h"

#include "HmumuAnahelpers/HmumuMiniTree.h"

HmumuMiniTree :: HmumuMiniTree(xAOD::TEvent * event, TTree* tree, TFile* file, float units, bool debug, xAOD::TStore* store) :
  HmumuTreeBase(event, tree, file, 1e3, debug, store)
{
   Info("HmumuMiniTree()", "Inside Constructor");
   Info("HmumuMiniTree()", "Creating output TTree %s", tree->GetName());
}


HmumuMiniTree :: ~HmumuMiniTree()
{
}


void HmumuMiniTree::AddEventUser(const std::string detailStr)
{
    makeBranch("Event","TruthHiggsDecay", m_decayPdgId);
}

std::string getStringDescriptor(std::string detailStr, std::string start, std::string end = " ", std::string delimiter = ":")
{
  //if the detailStr is "JVTWP:Medium IsolationWP:Loose "
  //start = IsolationWP
  //end = " "
  //delimiter = ":"

  //this line step finds the index of start in detailStr. This corresponds to "JVTWP:Medium >>I<<solationWP:Loose "
  unsigned first = detailStr.find(start);
  //including_start_string is now "IsolationWP:Loose "
  std::string including_start_string = detailStr.substr(first);

  //This finds the index of the delimiter in "IsolationWP:Loose ".
  unsigned delimiter_location = including_start_string.find(delimiter);
  //This creates the substring "Loose "
  std::string after_delimiter = including_start_string.substr(delimiter_location + 1);

  //This finds the first space in the string
  //"Loose>> <<"
  unsigned last = after_delimiter.find(end);

  //Returns the string without the space "Loose"
  return after_delimiter.substr(0,last);
}

std::map<std::string, std::string>* getMuonWPs(std::string detailStr)
{
    std::map<std::string, std::string>* efficiencyMap = new std::map<std::string, std::string>;
    if (detailStr.find("MuonIsoWP:") != std::string::npos)
    {
        (*efficiencyMap)["MuonIsoWP"] = getStringDescriptor(detailStr, "MuonIsoWP:", " ");
    }

    if (detailStr.find("MuonQualWP:") != std::string::npos)
    {
        (*efficiencyMap)["MuonQualWP"] = getStringDescriptor(detailStr, "MuonQualWP:", " ");
        std::cout<<"Quality string "+ (*efficiencyMap)["MuonQualWP"]<<std::endl;
    }

    if (detailStr.find("SystListReco:") != std::string::npos)
    {
        (*efficiencyMap)["SystListReco"] = getStringDescriptor(detailStr, "SystListReco:", " ");
    }
    if (detailStr.find("SystListTrig:") != std::string::npos)
    {
        (*efficiencyMap)["SystListTrig"] = getStringDescriptor(detailStr, "SystListTrig:", " ");
    }
    if (detailStr.find("SystListIso:") != std::string::npos)
    {
        (*efficiencyMap)["SystListIso"] = getStringDescriptor(detailStr, "SystListIso:", " ");
    }
    if (detailStr.find("SystListTTVA:") != std::string::npos)
    {
        (*efficiencyMap)["SystListTTVA"] = getStringDescriptor(detailStr, "SystListTTVA:", " ");
    }

    return efficiencyMap;
}

std::map<std::string, std::string>* getJetWPs(std::string detailStr)
{
    std::map<std::string, std::string>* efficiencyMap = new std::map<std::string, std::string>;
    if (detailStr.find("JetJVTWP:") != std::string::npos)
    {
        (*efficiencyMap)["JetJVTWP"] = getStringDescriptor(detailStr, "JetJVTWP:", " ");
    }
    if (detailStr.find("JetfJVTWP:") != std::string::npos)
    {
        (*efficiencyMap)["JetfJVTWP"] = getStringDescriptor(detailStr, "JetfJVTWP:", " ");
    }
    if (detailStr.find("SystListBJet:") != std::string::npos)
    {
        (*efficiencyMap)["SystListBJet"] = getStringDescriptor(detailStr, "SystListBJet:", " ");
    }
    if (detailStr.find("SystListJVT:") != std::string::npos)
    {
        (*efficiencyMap)["SystListJVT"] = getStringDescriptor(detailStr, "SystListJVT");
    }
    if (detailStr.find("SystListfJVT:") != std::string::npos)
    {
        (*efficiencyMap)["SystListfJVT"] = getStringDescriptor(detailStr, "SystListfJVT");
    }

    return efficiencyMap;
}

std::map<std::string, std::string>* getElectronWPs(std::string detailStr)
{
    std::map<std::string, std::string>* efficiencyMap = new std::map<std::string, std::string>;
    if (detailStr.find("ElectronQualWP:") != std::string::npos)
    {
        (*efficiencyMap)["ElectronQualWP"] = getStringDescriptor(detailStr, "ElectronQualWP:", " ");
    }

    if (detailStr.find("ElectronIsoWP:") != std::string::npos)
    {
        (*efficiencyMap)["ElectronIsoWP"] = getStringDescriptor(detailStr, "ElectronIsoWP:", " ");
    }

    if (detailStr.find("SystListPID:") != std::string::npos)
    {
        (*efficiencyMap)["SystListPID"] = getStringDescriptor(detailStr, "SystListPID:", " ");
    }

    if (detailStr.find("SystListIso:") != std::string::npos)
    {
        (*efficiencyMap)["SystListIso"] = getStringDescriptor(detailStr, "SystListIso:", " ");
    }

    if (detailStr.find("SystListReco:") != std::string::npos)
    {
        (*efficiencyMap)["SystListReco"] = getStringDescriptor(detailStr, "SystListReco:", " ");
    }

    return efficiencyMap;
}


void HmumuMiniTree::AddJetsUser(const std::string detailStr, const std::string jetName)
{
  //Prapare the Branches and accessors of all the variables that we need to read and store. Only jet-related variables are taken care of here"
  m_jetWPs = getJetWPs(detailStr);
  makeBranch("Jets", "Pt", m_selectedJets_pt);
  makeBranch("Jets", "Energy", m_selectedJets_e);
  makeBranch("Jets", "Eta", m_selectedJets_eta);
  makeBranch("Jets", "Phi", m_selectedJets_phi);

  makeBranch("Jets", "BTagWP60", m_jetsBTagWP60);
  makeBranch("Jets", "BTagWP70", m_jetsBTagWP70);
  makeBranch("Jets", "BTagWP77", m_jetsBTagWP77);
  makeBranch("Jets", "BTagWP85", m_jetsBTagWP85);
  makeBranch("Jets", "BTagWeight", m_jetsBTagWeight);
  makeBranch("Jets", "BTagQuantile", m_jetsBTagQuantile);
  //makeBranch("Jets", "TruthOrigin", m_selectedJets_TruthOrigin);
  //makeBranch("Jets", "TruthType", m_selectedJets_TruthType);

  m_jetJVTSF_Syst.resize(1);
  m_jetfJVTSF_Syst.resize(1);
  m_eventBTagSF60_Syst.resize(1);
  m_eventBTagSF70_Syst.resize(1);
  m_eventBTagSF77_Syst.resize(1);
  m_eventBTagSF85_Syst.resize(1);
  m_eventBTagSFContinuous_Syst.resize(1);
  m_eventBTagIneffSFContinuous_Syst.resize(1);
  m_jetJVTSF_Syst[0] = 1.0;
  m_jetfJVTSF_Syst[0] = 1.0;
  m_eventBTagSF60_Syst[0] = 1.0;
  m_eventBTagSF70_Syst[0] = 1.0;
  m_eventBTagSF77_Syst[0] = 1.0;
  m_eventBTagSF85_Syst[0] = 1.0;
  m_eventBTagSFContinuous_Syst[0] = 1.0;
  m_eventBTagIneffSFContinuous_Syst[0] = 1.0;

  if(m_tree->GetName() == "nominal")
  {
    std::vector<std::string>* JVTSysts(nullptr);
    std::vector<std::string>* fJVTSysts(nullptr);
    std::vector<std::string>* BJetSysts(nullptr);

    //Get the JVT systematics list
    if (m_store->retrieve(JVTSysts, (*m_jetWPs)["SystListJVT"]).isSuccess())
    {
      m_jetJVTSF_Syst.resize(JVTSysts->size());
      m_jetJVTSF_Syst[0] = 1.0;
      for (unsigned int i = 1; i < JVTSysts->size(); i++)
      {
        m_jetJVTSF_Syst[i]=1.0;
        makeBranch("Weight", ("JVT_SF" + (*m_jetWPs)["JetJVTWP"] + "_" + JVTSysts->at(i) ), m_jetJVTSF_Syst[i] );
      }
    }

    //Get the fJVT systematics list
    if (m_store->retrieve(fJVTSysts, (*m_jetWPs)["SystListfJVT"]).isSuccess())
    {
      m_jetfJVTSF_Syst.resize(fJVTSysts->size());
      m_jetfJVTSF_Syst[0] = 1.0;
      for (unsigned int i = 1; i < fJVTSysts->size(); i++)
      {
        m_jetfJVTSF_Syst[i]=1.0;
        makeBranch("Weight", ("fJVT_SF" + (*m_jetWPs)["JetfJVTWP"] + "_" + fJVTSysts->at(i) ), m_jetfJVTSF_Syst[i] );
      }
    }

    //Get the BJet systematics list
    if (m_store->retrieve(BJetSysts, (*m_jetWPs)["SystListBJet"]).isSuccess())
    {
      m_eventBTagSF60_Syst.resize( BJetSysts->size());
      m_eventBTagSF70_Syst.resize( BJetSysts->size());
      m_eventBTagSF77_Syst.resize( BJetSysts->size());
      m_eventBTagSF85_Syst.resize( BJetSysts->size());
      m_eventBTagSFContinuous_Syst.resize( BJetSysts->size());
      m_eventBTagIneffSFContinuous_Syst.resize( BJetSysts->size());

      m_eventBTagSF60_Syst[0] = 1.0;
      m_eventBTagSF70_Syst[0] = 1.0;
      m_eventBTagSF77_Syst[0] = 1.0;
      m_eventBTagSF85_Syst[0] = 1.0;
      m_eventBTagSFContinuous_Syst[0] = 1.0;
      m_eventBTagIneffSFContinuous_Syst[0] = 1.0;

      for (unsigned int i = 1; i < BJetSysts->size(); i++)
      {
        m_eventBTagSF60_Syst[i] = 1.0;
        makeBranch("Weight", "BTagSF60_" + BJetSysts->at(i), m_eventBTagSF60_Syst[i] );
        m_eventBTagSF70_Syst[i] = 1.0;
        makeBranch("Weight", "BTagSF70_" + BJetSysts->at(i), m_eventBTagSF70_Syst[i] );
        m_eventBTagSF77_Syst[i] = 1.0;
        makeBranch("Weight", "BTagSF77_" + BJetSysts->at(i), m_eventBTagSF77_Syst[i] );
        m_eventBTagSF85_Syst[i] = 1.0;
        makeBranch("Weight", "BTagSF85_" + BJetSysts->at(i), m_eventBTagSF85_Syst[i] );
        m_eventBTagSFContinuous_Syst[i] = 1.0;
        makeBranch("Weight", "BTagSFContinuous_" + BJetSysts->at(i), m_eventBTagSFContinuous_Syst[i] );
        m_eventBTagIneffSFContinuous_Syst[i] = 1.0;
        makeBranch("Weight", "BTagIneffSFContinuous_" + BJetSysts->at(i), m_eventBTagIneffSFContinuous_Syst[i] );
      }
    }
  }

  makeBranch("Weight", ("JVT_SF" + (*m_jetWPs)["JetJVTWP"]), m_jetJVTSF_Syst[0]);
  makeBranch("Weight", ("fJVT_SF" + (*m_jetWPs)["fJetJVTWP"]), m_jetfJVTSF_Syst[0]);
  makeBranch("Weight", "BTagSF60", m_eventBTagSF60_Syst[0]);
  makeBranch("Weight", "BTagSF70", m_eventBTagSF70_Syst[0]);
  makeBranch("Weight", "BTagSF77", m_eventBTagSF77_Syst[0]);
  makeBranch("Weight", "BTagSF85", m_eventBTagSF85_Syst[0]);
  makeBranch("Weight", "BTagSFContinuous", m_eventBTagSFContinuous_Syst[0]);
  makeBranch("Weight", "BTagIneffSFContinuous", m_eventBTagIneffSFContinuous_Syst[0]);

  m_accbtag_WP60 = new SG::AuxElement::ConstAccessor<char>("BTag_MV2c10_FixedCutBEff_60");
  m_accbtag_SF60 =  new SG::AuxElement::ConstAccessor<std::vector<float> >("BTag_SF_MV2c10_FixedCutBEff_60");
  m_accbtag_WP70 = new SG::AuxElement::ConstAccessor<char>("BTag_MV2c10_FixedCutBEff_70");
  m_accbtag_SF70 = new SG::AuxElement::ConstAccessor<std::vector<float> >("BTag_SF_MV2c10_FixedCutBEff_70");
  m_accbtag_WP77 = new SG::AuxElement::ConstAccessor<char>("BTag_MV2c10_FixedCutBEff_77");
  m_accbtag_SF77 = new SG::AuxElement::ConstAccessor<std::vector<float> >("BTag_SF_MV2c10_FixedCutBEff_77");
  m_accbtag_WP85 = new SG::AuxElement::ConstAccessor<char>("BTag_MV2c10_FixedCutBEff_85");
  m_accbtag_SF85 = new SG::AuxElement::ConstAccessor<std::vector<float> >("BTag_SF_MV2c10_FixedCutBEff_85");

  //All that new continuous btagging stuff!
  m_accbtag_weight = new SG::AuxElement::ConstAccessor<float>("BTag_Weight_MV2c10_Continuous");
  m_accbtag_quantile = new SG::AuxElement::ConstAccessor<int>("BTag_Quantile_MV2c10_Continuous");
  m_accbtag_SFContinuous = new SG::AuxElement::ConstAccessor<std::vector<float> >("BTag_SF_MV2c10_Continuous");
  m_accbtag_IneffSFContinuous = new SG::AuxElement::ConstAccessor<std::vector<float> >("BTag_InefficiencySF_MV2c10_Continuous");

  m_accjvt_SF = new SG::AuxElement::ConstAccessor<std::vector<float> >(("JVTSystList_JVT_" + (*m_jetWPs)["JetJVTWP"]));
  m_accfjvt_SF = new SG::AuxElement::ConstAccessor<std::vector<float> >(("fJVTSystList_fJVT_" + (*m_jetWPs)["JetfJVTWP"]));
  //m_acc_jetTruthOrigin = new SG::AuxElement::ConstAccessor< int > ("truthOrigin");
  //m_acc_jetTruthType = new SG::AuxElement::ConstAccessor< int > ("truthType");


}

void HmumuMiniTree::AddElectronsUser(const std::string detailStr, const std::string elecName)
{

  //Prepare the Branches and accessors of all the electron related variables that we need to read"
  m_electronWPs = getElectronWPs(detailStr);

  makeBranch("ElectronsPos", "Pt", m_selectedElectronsPos_pt);
  makeBranch("ElectronsPos", "Energy", m_selectedElectronsPos_e);
  makeBranch("ElectronsPos", "Eta", m_selectedElectronsPos_eta);
  makeBranch("ElectronsPos", "Phi", m_selectedElectronsPos_phi);
  makeBranch("ElectronsPos", "Veto", m_selectedElectronsPos_veto);
  makeBranch("ElectronsPos", "Iso", m_selectedElectronsPos_iso);
  makeBranch("ElectronsPos", "TruthOrigin", m_selectedElectronsPos_TruthOrigin);
  makeBranch("ElectronsPos", "TruthType", m_selectedElectronsPos_TruthType);

  makeBranch("ElectronsNeg", "Pt", m_selectedElectronsNeg_pt);
  makeBranch("ElectronsNeg", "Energy", m_selectedElectronsNeg_e);
  makeBranch("ElectronsNeg", "Eta", m_selectedElectronsNeg_eta);
  makeBranch("ElectronsNeg", "Phi", m_selectedElectronsNeg_phi);
  makeBranch("ElectronsNeg", "Veto", m_selectedElectronsNeg_veto);
  makeBranch("ElectronsNeg", "Iso", m_selectedElectronsNeg_iso);
  makeBranch("ElectronsNeg", "TruthOrigin", m_selectedElectronsNeg_TruthOrigin);
  makeBranch("ElectronsNeg", "TruthType", m_selectedElectronsNeg_TruthType);

  m_electronPIDSFString = "ElPIDEff_SF_syst_" + (*m_electronWPs)["ElectronQualWP"];
  m_electronIsoSFString = "ElIsoEff_SF_syst_" + (*m_electronWPs)["ElectronQualWP"] + "_isol" + (*m_electronWPs)["ElectronIsoWP"];
  m_electronQualSFString = "ElRecoEff_SF_syst_" + (*m_electronWPs)["ElectronQualWP"];

  //prepare the nominal case
  m_electronQualSF_Syst.resize(1);
  m_electronQualSF_Syst[0] = 1.0;
  m_electronIsoSF_Syst.resize(1);
  m_electronIsoSF_Syst[0] = 1.0;
  m_electronPIDSF_Syst.resize(1);
  m_electronPIDSF_Syst[0] = 1.0;

  //prepare the systematics
  if(m_tree->GetName() == "nominal")
  {
    std::vector<std::string>* IsoSysts(nullptr);
    std::vector<std::string>* RecoSysts(nullptr);
    std::vector<std::string>* PIDSysts(nullptr);
    if ( (m_store->retrieve(RecoSysts, (*m_electronWPs)["SystListReco"])).isSuccess() )
    {
      m_electronQualSF_Syst.resize(RecoSysts->size());
      m_electronQualSF_Syst[0] = 1.0;
      for (unsigned int i = 1; i < RecoSysts->size(); i++)
      {
        m_electronQualSF_Syst[i] = 1.0;
        makeBranch("Weight", ("ElectronQualSF" + (*m_electronWPs)["ElectronRecoWP"] + "_" + RecoSysts->at(i) ), m_electronQualSF_Syst[i] );
      }
    }
    if ( (m_store->retrieve(IsoSysts, (*m_electronWPs)["SystListIso"])).isSuccess() )
    {
      m_electronIsoSF_Syst.resize(IsoSysts->size());
      m_electronIsoSF_Syst[0] = 1.0;
      for (unsigned int i = 1; i < IsoSysts->size(); i++)
      {
        m_electronIsoSF_Syst[i] = 1.0;
        makeBranch("Weight", ("ElectronIsoSF" + (*m_electronWPs)["ElectronIsoWP"] + "_" + IsoSysts->at(i) ), m_electronIsoSF_Syst[i] );
      }
    }
    if ( (m_store->retrieve(PIDSysts, (*m_electronWPs)["SystListPID"])).isSuccess() )
    {
      m_electronPIDSF_Syst.resize(PIDSysts->size());
      m_electronPIDSF_Syst[0] = 1.0;
      for (unsigned int i = 1; i < PIDSysts->size(); i++)
      {
        m_electronPIDSF_Syst[i] = 1.0;
        makeBranch("Weight", ("ElectronPIDSF" + (*m_electronWPs)["ElectronQualWP"] + "_" + PIDSysts->at(i) ), m_electronPIDSF_Syst[i] );
      }
    }
  }

  makeBranch("Weight", ("ElectronIsoSF" + (*m_electronWPs)["ElectronIsoWP"]), m_electronIsoSF_Syst[0]);
  makeBranch("Weight", ("ElectronQualSF" + (*m_electronWPs)["ElectronQualWP"]), m_electronPIDSF_Syst[0]);
  makeBranch("Weight", ("ElectronQualSF"), m_electronQualSF_Syst[0]);

  m_acc_electronPIDSF = new SG::AuxElement::ConstAccessor<std::vector<float> > ((m_electronPIDSFString));
  m_acc_electronIsoSF = new SG::AuxElement::ConstAccessor<std::vector<float> > ((m_electronIsoSFString));
  m_acc_electronQualSF = new SG::AuxElement::ConstAccessor<std::vector<float> > ((m_electronQualSFString));

  m_acc_electronPromptIso = new SG::AuxElement::ConstAccessor< float > ("PromptLeptonIso");
  m_acc_electronPromptVeto = new SG::AuxElement::ConstAccessor<  float > ("PromptLeptonVeto");
  m_acc_electronTruthOrigin = new SG::AuxElement::ConstAccessor< int > ("truthOrigin");
  m_acc_electronTruthType = new SG::AuxElement::ConstAccessor< int > ("truthType");
}

void HmumuMiniTree::AddMuonsUser(const std::string detailStr, const std::string muName)
{

  //Prepare the branches and accessors of all the muon related variables that we need to read
  m_muonWPs = getMuonWPs(detailStr);

  makeBranch("Event", "TrigMatched_HLT_mu20_iloose_L1MU15", m_TrigMatch_HLT_mu20_iloose_L1MU15);
  makeBranch("Event", "TrigMatched_HLT_mu26_ivarmedium", m_TrigMatch_HLT_mu26_ivarmedium);
  makeBranch("Event", "TrigMatched_HLT_mu50", m_TrigMatch_HLT_mu50);

  makeBranch("MuonsPos", "Pt", m_selectedMuonsPos_pt);
  makeBranch("MuonsPos", "Energy", m_selectedMuonsPos_e);
  makeBranch("MuonsPos", "Eta", m_selectedMuonsPos_eta);
  makeBranch("MuonsPos", "Phi", m_selectedMuonsPos_phi);
  makeBranch("MuonsPos", "Veto", m_selectedMuonsPos_veto);
  makeBranch("MuonsPos", "Iso", m_selectedMuonsPos_iso);
  makeBranch("MuonsPos", "TruthOrigin", m_selectedMuonsPos_TruthOrigin);
  makeBranch("MuonsPos", "TruthType", m_selectedMuonsPos_TruthType);

  makeBranch("MuonsNeg", "Pt", m_selectedMuonsNeg_pt);
  makeBranch("MuonsNeg", "Energy", m_selectedMuonsNeg_e);
  makeBranch("MuonsNeg", "Eta", m_selectedMuonsNeg_eta);
  makeBranch("MuonsNeg", "Phi", m_selectedMuonsNeg_phi);
  makeBranch("MuonsNeg", "Veto", m_selectedMuonsNeg_veto);
  makeBranch("MuonsNeg", "Iso", m_selectedMuonsNeg_iso);
  makeBranch("MuonsNeg", "TruthOrigin", m_selectedMuonsNeg_TruthOrigin);
  makeBranch("MuonsNeg", "TruthType", m_selectedMuonsNeg_TruthType);

  m_muonQualSFString =  "MuRecoEff_SF_syst_Reco" + (*m_muonWPs)["MuonQualWP"];
  m_muonIsoSFString = "MuIsoEff_SF_syst_Iso" + (*m_muonWPs)["MuonIsoWP"];
  m_muonTriggerHLT26SFString = "MuTrigEff_SF_syst_HLT_mu26_ivarmedium_OR_HLT_mu50_Reco" + (*m_muonWPs)["MuonQualWP"];
  m_muonTriggerHLT26EffString = "MuTrigMCEff_syst_HLT_mu26_ivarmedium_OR_HLT_mu50_Reco" + (*m_muonWPs)["MuonQualWP"];
  m_muonTriggerHLT20SFString = "MuTrigEff_SF_syst_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_Reco" + (*m_muonWPs)["MuonQualWP"];
  m_muonTriggerHLT20EffString = "MuTrigMCEff_syst_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_Reco" + (*m_muonWPs)["MuonQualWP"];
  m_muonTrigMatchString = "isTrigMatchedMapMu";

  m_muonIsoSF_Syst.resize(1);
  m_muonIsoSF_Syst[0] = 1.0;
  m_muonQualSF_Syst.resize(1);
  m_muonQualSF_Syst[0] = 1.0;

  m_muonTrigSF_HLT_mu26_ivarmedium_OR_HLT_mu50_Syst.resize(1);
  m_muonTrigSF_HLT_mu26_ivarmedium_OR_HLT_mu50_Syst[0] = 1.0;
  m_muonTrigSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_Syst.resize(1);
  m_muonTrigSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_Syst[0] = 1.0;

  m_muonTrigHLT26SF_Syst.push_back(std::vector<float>());
  m_muonTrigHLT26Eff_Syst.push_back(std::vector<float>());
  m_muonTrigHLT20SF_Syst.push_back(std::vector<float>());
  m_muonTrigHLT20Eff_Syst.push_back(std::vector<float>());

  if(m_tree->GetName() == "nominal")
  {
    std::vector<std::string>* IsoSysts(nullptr);
    std::vector<std::string>* RecoSysts(nullptr);
    std::vector<std::string>* TrigSysts(nullptr);
    if (m_store->retrieve(RecoSysts, (*m_muonWPs)["SystListReco"]).isSuccess())
    {
      m_muonQualSF_Syst.resize( RecoSysts->size() );
      m_muonQualSF_Syst[0] = 1.0;
      for (unsigned int i = 1; i < RecoSysts->size(); i++)
      {
        m_muonQualSF_Syst[i] = 1.0;
        makeBranch("Weight", ("MuonQualSF" + (*m_muonWPs)["MuonRecoWP"] + "_" + RecoSysts->at(i)), m_muonQualSF_Syst[i] );
      }
    }
    if (m_store->retrieve(IsoSysts, (*m_muonWPs)["SystListIso"]).isSuccess())
    {
      m_muonIsoSF_Syst.resize( IsoSysts->size() );
      m_muonIsoSF_Syst[0] = 1.0;
      for (unsigned int i = 1; i < IsoSysts->size(); i++)
      {
        m_muonIsoSF_Syst[i] = 1.0;
        makeBranch("Weight", ("MuonIsoSF" + (*m_muonWPs)["MuonIsoWP"] + "_" + IsoSysts->at(i) ), m_muonIsoSF_Syst[i] );
      }
    }

    if (m_store->retrieve(TrigSysts, (*m_muonWPs)["SystListTrig"]).isSuccess())
    {
      m_muonTrigSF_HLT_mu26_ivarmedium_OR_HLT_mu50_Syst.resize(TrigSysts->size());
      m_muonTrigSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_Syst.resize(TrigSysts->size());
      m_muonTrigSF_HLT_mu26_ivarmedium_OR_HLT_mu50_Syst[0] = 1.0;
      m_muonTrigSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_Syst[0] = 1.0;
      
      for (unsigned int i = 1; i < TrigSysts->size(); i++)
      {
        m_muonTrigSF_HLT_mu26_ivarmedium_OR_HLT_mu50_Syst[i] = 1.0;
        m_muonTrigSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_Syst[i] = 1.0;
        makeBranch("Weight", ("MuonTrigSF_HLT_mu26_ivarmedium_OR_HLT_mu50_" + TrigSysts->at(i) ), m_muonTrigSF_HLT_mu26_ivarmedium_OR_HLT_mu50_Syst[i] );
        makeBranch("Weight", ("MuonTrigSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_" + TrigSysts->at(i) ), m_muonTrigSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_Syst[i] );
        m_muonTrigHLT26SF_Syst.push_back(std::vector<float>());
        m_muonTrigHLT26Eff_Syst.push_back(std::vector<float>());
        m_muonTrigHLT20SF_Syst.push_back(std::vector<float>());
        m_muonTrigHLT20Eff_Syst.push_back(std::vector<float>());
      }

    }
  }

  makeBranch("Weight", ("MuonIsoSF" + (*m_muonWPs)["MuonIsoWP"]), m_muonIsoSF_Syst[0]);
  makeBranch("Weight", ("MuonQualSF" + (*m_muonWPs)["MuonQualWP"]), m_muonQualSF_Syst[0]);
  makeBranch("Weight", ("MuonTrigSF_HLT_mu26_ivarmedium_OR_HLT_mu50"), m_muonTrigSF_HLT_mu26_ivarmedium_OR_HLT_mu50_Syst[0]);
  makeBranch("Weight", ("MuonTrigSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50"), m_muonTrigSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_Syst[0]);

   m_acc_muonQualSF = new SG::AuxElement::ConstAccessor<std::vector<float> > ((m_muonQualSFString));
   m_acc_muonIsoSF =  new SG::AuxElement::ConstAccessor<std::vector<float> >((m_muonIsoSFString));
   m_acc_muonTriggerHLT26SF =  new SG::AuxElement::ConstAccessor<std::vector<float> > ((m_muonTriggerHLT26SFString));
   m_acc_muonTriggerHLT26Eff =  new SG::AuxElement::ConstAccessor<std::vector<float> > ((m_muonTriggerHLT26EffString));
   m_acc_muonTriggerHLT20SF =  new SG::AuxElement::ConstAccessor<std::vector<float> > ((m_muonTriggerHLT20SFString));
   m_acc_muonTriggerHLT20Eff =  new SG::AuxElement::ConstAccessor<std::vector<float> > ((m_muonTriggerHLT20EffString));
   m_acc_muonTriggerMatch = new SG::AuxElement::ConstAccessor< std::map<std::string,char> > (m_muonTrigMatchString);

   m_acc_muonPromptIso = new SG::AuxElement::ConstAccessor< float > ("PromptLeptonIso");
   m_acc_muonPromptVeto = new SG::AuxElement::ConstAccessor< float > ("PromptLeptonVeto");
   m_acc_muonTruthOrigin = new SG::AuxElement::ConstAccessor< int > ("truthOrigin");
   m_acc_muonTruthType = new SG::AuxElement::ConstAccessor< int > ("truthType");
}

void HmumuMiniTree::FillEventUser( const xAOD::EventInfo* eventInfo ) {
    m_decayPdgId = eventInfo->auxdecor<int>("HiggsChildrenPDGID");

}

float GeneralSF(float SF)
{
    if (SF > 0.0) return SF;
    else return 1.0;
}

//calculate the trigger scale factors
float CalculateTrigSF(std::vector<float> &SF, std::vector<float> &Eff, std::vector<float> &pt, float plateau = 27e3)
{
    float not_P_MC = 1.0;
    float not_P_DATA = 1.0;
    for (unsigned int i = 0; i < SF.size(); i++){
        if (pt[i] > plateau * 1.05){
        not_P_DATA *= (1.0 - SF[i] * Eff[i]); //the efficiency is the MC eff. SF = effData/effMC. Therefore, effData = effMC * SF. 
        not_P_MC *= (1.0 - Eff[i]);
        }
    }
    if ((not_P_DATA != 1.0) and (not_P_MC != 1.0)) return (1.0 - not_P_DATA)/(1.0 - not_P_MC); 
    //Set the scale factor exatly to one in cases where we cannot compute the trigger scale factor. This is a bit nicer than setting it to 0, because we can always apply the absolute value of the trigger scale factor as a weight. If we want to check what events did not get a scale factor, we can check which events were negative.
    else return -1.0;
}

void HmumuMiniTree::FillJetsUser( const xAOD::Jet* jet, const std::string) {

    //Fill all of the jet related varialbes
    bool JVTSF_available = (*m_accjvt_SF).isAvailable(*jet);
    bool fJVTSF_available = (*m_accfjvt_SF).isAvailable(*jet);
    bool SF60_available = (*m_accbtag_WP60).isAvailable(*jet);
    bool SF70_available = (*m_accbtag_WP70).isAvailable(*jet);
    bool SF77_available = (*m_accbtag_WP77).isAvailable(*jet); 
    bool SF85_available = (*m_accbtag_WP85).isAvailable(*jet); 
    bool SFContinuous_available = (*m_accbtag_SFContinuous).isAvailable(*jet); 

    char btag_60 = (*m_accbtag_WP60)(*jet);
    char btag_70 = (*m_accbtag_WP70)(*jet);
    char btag_77 = (*m_accbtag_WP77)(*jet);
    char btag_85 = (*m_accbtag_WP85)(*jet);

    float btag_weight = (*m_accbtag_weight)(*jet);
    int btag_quantile = (*m_accbtag_quantile)(*jet);

    std::vector<float> btag_SF60;
    std::vector<float> btag_SF70;
    std::vector<float> btag_SF77;
    std::vector<float> btag_SF85;

    std::vector<float> btag_SFContinuous;
    std::vector<float> btag_IneffSFContinuous;

    std::vector<float> JVT_SF;
    std::vector<float> fJVT_SF;

    if (SF60_available) btag_SF60 = (*m_accbtag_SF60)(*jet);
    if (SF70_available) btag_SF70 = (*m_accbtag_SF70)(*jet);
    if (SF77_available) btag_SF77 = (*m_accbtag_SF77)(*jet);
    if (SF85_available) btag_SF85 = (*m_accbtag_SF85)(*jet);
    if (SFContinuous_available) btag_SFContinuous = (*m_accbtag_SFContinuous)(*jet);
    if (SFContinuous_available) btag_IneffSFContinuous = (*m_accbtag_IneffSFContinuous)(*jet);
    if (JVTSF_available) JVT_SF = (*m_accjvt_SF)(*jet);
    if (fJVTSF_available) fJVT_SF = (*m_accfjvt_SF)(*jet);

    if (SF60_available){
    for (unsigned int i = 0; i < m_eventBTagSF60_Syst.size(); i++){
      m_eventBTagSF60_Syst[i] = m_eventBTagSF60_Syst[i] * btag_SF60[i];
    }
    }

    if (SF70_available){
    for (unsigned int i = 0; i < m_eventBTagSF70_Syst.size(); i++){
      m_eventBTagSF70_Syst[i] = m_eventBTagSF70_Syst[i] * btag_SF70[i];
    }
    }

    if (SF77_available){
    for (unsigned int i = 0; i < m_eventBTagSF77_Syst.size(); i++){
      m_eventBTagSF77_Syst[i] = m_eventBTagSF77_Syst[i] * btag_SF77[i];
    }
    }

    if (SF85_available){
    for (unsigned int i = 0; i < m_eventBTagSF85_Syst.size(); i++){
      m_eventBTagSF85_Syst[i] = m_eventBTagSF85_Syst[i] * btag_SF85[i];
    }
    }

    if (SFContinuous_available){
    for (unsigned int i = 0; i < m_eventBTagSFContinuous_Syst.size(); i++){
      m_eventBTagSFContinuous_Syst[i] = m_eventBTagSFContinuous_Syst[i] * btag_SFContinuous[i];
    }
    }

    if (SFContinuous_available){
    for (unsigned int i = 0; i < m_eventBTagSFContinuous_Syst.size(); i++){
      m_eventBTagIneffSFContinuous_Syst[i] = m_eventBTagIneffSFContinuous_Syst[i] * btag_IneffSFContinuous[i];
    }
    }

    if (JVTSF_available){
    for (unsigned int i = 0; i < m_jetJVTSF_Syst.size(); i ++){
      m_jetJVTSF_Syst[i] = m_jetJVTSF_Syst[i] * JVT_SF[i];
    }
    }
    if (fJVTSF_available){
    for (unsigned int i = 0; i < m_jetfJVTSF_Syst.size(); i ++){
      m_jetfJVTSF_Syst[i] = m_jetfJVTSF_Syst[i] * fJVT_SF[i];
    }
    }

    //m_selectedJets_TruthOrigin.push_back( (*m_acc_jetTruthOrigin)(*jet));
    //m_selectedJets_TruthType.push_back( (*m_acc_jetTruthType)(*jet));

    m_jetsBTagWeight.push_back(btag_weight);
    m_jetsBTagQuantile.push_back(btag_quantile);
    m_jetsBTagWP60.push_back(btag_60);
    m_jetsBTagWP70.push_back(btag_70);
    m_jetsBTagWP77.push_back(btag_77);
    m_jetsBTagWP85.push_back(btag_85);

    m_selectedJets_pt.push_back ( jet->pt() / m_units );
	m_selectedJets_eta.push_back( jet->eta() );
	m_selectedJets_phi.push_back( jet->phi() );
	m_selectedJets_e.push_back  ( jet->e() / m_units );

}

void HmumuMiniTree::FillMuonsUser( const xAOD::Muon* muon, const std::string detailStr) {

    //are the scale factors available?
    bool qualSF_available = (*m_acc_muonQualSF).isAvailable(*muon);
    bool isoSF_available = (*m_acc_muonIsoSF).isAvailable(*muon);
    bool TrigHLT26SF_available = (*m_acc_muonTriggerHLT26SF).isAvailable(*muon);
    bool TrigHLT26Eff_available = (*m_acc_muonTriggerHLT26Eff).isAvailable(*muon); 
    bool TrigHLT20SF_available = (*m_acc_muonTriggerHLT20SF).isAvailable(*muon); 
    bool TrigHLT20Eff_available = (*m_acc_muonTriggerHLT20Eff).isAvailable(*muon);

    // instances to store the SF vectors
    std::vector<float> qualSF;
    std::vector<float> isoSF;
    std::vector<float> TrigHLT26SF;
    std::vector<float> TrigHLT26Eff;
    std::vector<float> TrigHLT20SF;
    std::vector<float> TrigHLT20Eff;

    // access the vectors if they are available
    if (qualSF_available) qualSF = (*m_acc_muonQualSF)(*muon);
    if (isoSF_available) isoSF = (*m_acc_muonIsoSF)(*muon);
    if (TrigHLT26SF_available) TrigHLT26SF = (*m_acc_muonTriggerHLT26SF)(*muon);
    if (TrigHLT26Eff_available) TrigHLT26Eff = (*m_acc_muonTriggerHLT26Eff)(*muon);
    if (TrigHLT20SF_available) TrigHLT20SF = (*m_acc_muonTriggerHLT20SF)(*muon);
    if (TrigHLT20Eff_available) TrigHLT20Eff = (*m_acc_muonTriggerHLT20Eff)(*muon);

    //get the systematic variations of all of the weights
    if (qualSF_available){
    for (unsigned int i = 0; i < m_muonQualSF_Syst.size(); i++)
    {
      m_muonQualSF_Syst[i] = m_muonQualSF_Syst[i] * qualSF[i];
    }
    }
    if (isoSF_available){
    for (unsigned int i = 0; i < m_muonIsoSF_Syst.size(); i++)
    {
      m_muonIsoSF_Syst[i] = m_muonIsoSF_Syst[i] * isoSF[i];
    }
    }

    //I think that m_muonTrigSF_HLT_mu26_ivarmedium_OR_HLT_mu50_Syst and m_muonTrigSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_Syst have different lengths...
    if(TrigHLT26SF_available){
    for (unsigned int i = 0; i < m_muonTrigSF_HLT_mu26_ivarmedium_OR_HLT_mu50_Syst.size(); i ++)
    {
      m_muonTrigHLT26SF_Syst[i].push_back(TrigHLT26SF[i]);
      m_muonTrigHLT26Eff_Syst[i].push_back(TrigHLT26Eff[i]);
    }
    }
    if(TrigHLT20SF_available){
    for (unsigned int i = 0; i < m_muonTrigSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_Syst.size(); i ++)
    {
      m_muonTrigHLT20SF_Syst[i].push_back(TrigHLT20SF[i]);
      m_muonTrigHLT20Eff_Syst[i].push_back(TrigHLT20Eff[i]);
    }
    }
    m_muonPt.push_back(muon->pt());

    //trigger matching map
    std::map<std::string, char> trigger_map = (*m_acc_muonTriggerMatch)(*muon);
    if (trigger_map.find("HLT_mu20_iloose_L1MU15") != trigger_map.end())
    {
         m_TrigMatch_HLT_mu20_iloose_L1MU15 = (int) ( ( (bool) m_TrigMatch_HLT_mu20_iloose_L1MU15 ) or ( (bool) trigger_map["HLT_mu20_iloose_L1MU15"] ) );
         m_vec_TrigMatch_HLT_mu20_iloose_L1MU15.push_back((int) ( (bool) trigger_map["HLT_mu20_iloose_L1MU15"]));
    }

    if (trigger_map.find("HLT_mu26_ivarmedium") != trigger_map.end())
    {
         m_TrigMatch_HLT_mu26_ivarmedium = (int) ( ( (bool) m_TrigMatch_HLT_mu26_ivarmedium ) or ( (bool) trigger_map["HLT_mu26_ivarmedium"] ) );
         m_vec_TrigMatch_HLT_mu26_ivarmedium.push_back( (int) ( (bool ) trigger_map["HLT_mu26_ivarmedium"]));
    }

    if (trigger_map.find("HLT_mu50") != trigger_map.end())
    {
        m_TrigMatch_HLT_mu50 = (int) ( ( (bool) m_TrigMatch_HLT_mu50)  or ( (bool) trigger_map["HLT_mu50"]));
        m_vec_TrigMatch_HLT_mu50.push_back((int) ( (bool) trigger_map["HLT_mu50"]));
    }

    if (muon->charge() > 0.0){
      m_selectedMuonsPos_pt.push_back ( muon->pt() / m_units );
	    m_selectedMuonsPos_eta.push_back( muon->eta() );
	    m_selectedMuonsPos_phi.push_back( muon->phi() );
	    m_selectedMuonsPos_e.push_back  ( muon->e() / m_units );
      if ( (*m_acc_muonPromptIso).isAvailable(*muon)) m_selectedMuonsPos_iso.push_back( (*m_acc_muonPromptIso)(*muon));
      else m_selectedMuonsPos_iso.push_back(2.0);
      if ( (*m_acc_muonTruthOrigin).isAvailable(*muon)) m_selectedMuonsPos_TruthOrigin.push_back((*m_acc_muonTruthOrigin)(*muon));
      else m_selectedMuonsPos_TruthOrigin.push_back(0);
      if ( (*m_acc_muonTruthType).isAvailable(*muon)) m_selectedMuonsPos_TruthType.push_back((*m_acc_muonTruthType)(*muon));
      else m_selectedMuonsPos_TruthType.push_back(0);

      if ( (*m_acc_muonPromptVeto).isAvailable(*muon)) m_selectedMuonsPos_veto.push_back( (*m_acc_muonPromptVeto)(*muon));
      else m_selectedMuonsPos_veto.push_back(2.0);
    }

    else if (muon->charge()< 0.0){
      m_selectedMuonsNeg_pt .push_back ( muon->pt() / m_units );
	    m_selectedMuonsNeg_eta.push_back( muon->eta() );
	    m_selectedMuonsNeg_phi.push_back( muon->phi() );
  	  m_selectedMuonsNeg_e.push_back  ( muon->e() / m_units );
      if ( (*m_acc_muonPromptIso).isAvailable(*muon)) m_selectedMuonsNeg_iso.push_back( (*m_acc_muonPromptIso)(*muon));
      else m_selectedMuonsNeg_iso.push_back(2.0);
      if ( (*m_acc_muonPromptVeto).isAvailable(*muon)) m_selectedMuonsNeg_veto.push_back( (*m_acc_muonPromptVeto)(*muon));
      else m_selectedMuonsNeg_veto.push_back(2.0);
      if ( (*m_acc_muonTruthOrigin).isAvailable(*muon)) m_selectedMuonsNeg_TruthOrigin.push_back((*m_acc_muonTruthOrigin)(*muon));
      else m_selectedMuonsNeg_TruthOrigin.push_back(0);
      if ( (*m_acc_muonTruthType).isAvailable(*muon)) m_selectedMuonsNeg_TruthType.push_back((*m_acc_muonTruthType)(*muon));
      else m_selectedMuonsNeg_TruthType.push_back(0);
    }

    else{std::cout<<"Couldn't get the charge() of the muon"<<std::endl;}

}

void HmumuMiniTree::FinalizeMuonsUser(){
    for (unsigned int i=0; i < m_muonTrigSF_HLT_mu26_ivarmedium_OR_HLT_mu50_Syst.size(); i++)
    {
      m_muonTrigSF_HLT_mu26_ivarmedium_OR_HLT_mu50_Syst[i] = CalculateTrigSF(m_muonTrigHLT26SF_Syst[i], m_muonTrigHLT26Eff_Syst[i], m_muonPt, 26e3);
      m_muonTrigSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_Syst[i] = CalculateTrigSF(m_muonTrigHLT20SF_Syst[i], m_muonTrigHLT20Eff_Syst[i], m_muonPt, 20e3);
    }
}

void HmumuMiniTree::FillElectronsUser( const xAOD::Electron* el, const std::string detailStr ) {

    bool PIDSF_available = (*m_acc_electronPIDSF).isAvailable(*el);
    bool isoSF_available = (*m_acc_electronIsoSF).isAvailable(*el);
    bool recoSF_available = (*m_acc_electronQualSF).isAvailable(*el);

    std::vector<float> PIDSF;
    if (PIDSF_available) PIDSF = (*m_acc_electronPIDSF)(*el);
    std::vector<float> isoSF;
    if (isoSF_available) isoSF = (*m_acc_electronIsoSF)(*el);
    std::vector<float> QualSF;
    if (recoSF_available) QualSF = (*m_acc_electronQualSF)(*el);

    if (PIDSF_available){
      for (unsigned int i =0; i < m_electronPIDSF_Syst.size(); i++)
      {
        m_electronPIDSF_Syst[i] = m_electronPIDSF_Syst[i] * PIDSF[i];
      }
    }
    if (isoSF_available){
      for (unsigned int i =0; i < m_electronIsoSF_Syst.size(); i++)
      {
        m_electronIsoSF_Syst[i] = m_electronIsoSF_Syst[i] * isoSF[i];
      }
    }
    if(recoSF_available){
      for (unsigned int i =0; i < m_electronQualSF_Syst.size(); i++)
      {
        m_electronQualSF_Syst[i] = m_electronQualSF_Syst[i] * QualSF[i];
      }
    }

    if (el->charge() > 0.0){
      m_selectedElectronsPos_pt.push_back ( el->pt() / m_units );
	    m_selectedElectronsPos_eta.push_back( el->eta() );
	    m_selectedElectronsPos_phi.push_back( el->phi() );
	    m_selectedElectronsPos_e.push_back  ( el->e() / m_units );
      if ( (*m_acc_electronPromptIso).isAvailable(*el)) m_selectedElectronsPos_iso.push_back( (*m_acc_electronPromptIso)(*el));
      else m_selectedElectronsPos_iso.push_back(2.0);
      if ( (*m_acc_electronPromptVeto).isAvailable(*el)) m_selectedElectronsPos_veto.push_back( (*m_acc_electronPromptVeto)(*el));
      else m_selectedElectronsPos_veto.push_back(2.0);
      if ( (* m_acc_electronTruthOrigin).isAvailable(*el)) m_selectedElectronsPos_TruthOrigin.push_back((*m_acc_electronTruthOrigin)(*el));
      else m_selectedElectronsPos_TruthOrigin.push_back(0);
      if ( (* m_acc_electronTruthType).isAvailable(*el)) m_selectedElectronsPos_TruthType.push_back((*m_acc_electronTruthType)(*el));
      else m_selectedElectronsPos_TruthType.push_back(0);
    }
    else if (el->charge()< 0.0){
      m_selectedElectronsNeg_pt.push_back ( el->pt() / m_units );
	    m_selectedElectronsNeg_eta.push_back( el->eta() );
	    m_selectedElectronsNeg_phi.push_back( el->phi() );
	    m_selectedElectronsNeg_e.push_back  ( el->e() / m_units );
      if ( (*m_acc_electronPromptIso).isAvailable(*el)) m_selectedElectronsNeg_iso.push_back( (*m_acc_electronPromptIso)(*el));
      else m_selectedElectronsNeg_iso.push_back(2.0);
      if ( (*m_acc_electronPromptVeto).isAvailable(*el)) m_selectedElectronsNeg_veto.push_back( (*m_acc_electronPromptVeto)(*el));
      else m_selectedElectronsNeg_veto.push_back(2.0);
      if ( (*m_acc_electronTruthOrigin).isAvailable(*el)) m_selectedElectronsNeg_TruthOrigin.push_back((*m_acc_electronTruthOrigin)(*el));
      else m_selectedElectronsNeg_TruthOrigin.push_back(0);
      if ( (* m_acc_electronTruthType).isAvailable(*el)) m_selectedElectronsNeg_TruthType.push_back((*m_acc_electronTruthType)(*el));
      else m_selectedElectronsNeg_TruthType.push_back(0);
    }
    else{std::cout<<"Couldn't get the charge() of the electron"<<std::endl;}
}

void HmumuMiniTree::makeBranch(const TString & label, const TString & varname, std::vector<unsigned int> & var)
{
  std::cout<<"Making branch "  + label+"_"+varname<<std::endl;
    m_tree->Branch(label+"_"+varname, &var, label+"_"+varname);
}

void HmumuMiniTree::makeBranch(const TString & label, const TString & varname, char & var)
{
  std::cout<<"Making branch "  + label+"_"+varname<<std::endl;
    m_tree->Branch(label+"_"+varname, &var, label+"_"+varname);
}

void HmumuMiniTree::makeBranch(const TString & label, const TString & varname, int & var)
{
  std::cout<<"Making branch "  + label+"_"+varname<<std::endl;
  m_tree->Branch(label+"_"+varname, &var, label+"_"+varname+"/I");
}

void HmumuMiniTree::makeBranch(const TString & label, const TString & varname, float & var)
{
  std::cout<<"Making branch "  + label+"_"+varname<<std::endl;
  m_tree->Branch(label+"_"+varname, &var, label+"_"+varname+"/F");
}

void HmumuMiniTree::makeBranch(const TString & label, const TString & varname, double & var)
{
  std::cout<<"Making branch "  + label+"_"+varname<<std::endl;
  m_tree->Branch(label+"_"+varname, &var, label+"_"+varname+"/D");
}

void HmumuMiniTree::makeBranch(const TString & label, const TString & varname, std::vector<int> & var)
{
  std::cout<<"Making branch "  + label+"_"+varname<<std::endl;
  m_tree->Branch(label+"_"+varname, &var);
}

void HmumuMiniTree::makeBranch(const TString & label, const TString & varname, std::vector<float> & var)
{
  std::cout<<"Making branch "  + label+"_"+varname<<std::endl;
  m_tree->Branch(label+"_"+varname, &var);
}

void HmumuMiniTree::makeBranch(const TString & label, const TString & varname, std::vector<double> & var)
{
  std::cout<<"Making branch "  + label+"_"+varname<<std::endl;
  m_tree->Branch(label+"_"+varname, &var);
}

void HmumuMiniTree::makeBranch(const TString & label, const TString & varname, std::vector<char> & var)
{
  std::cout<<"Making branch "  + label+"_"+varname<<std::endl;
  m_tree->Branch(label+"_"+varname, &var);
}



void HmumuMiniTree::ClearJetsUser(const std::string jetName ) {
    m_selectedJets_pt.clear();
	m_selectedJets_eta.clear();
	m_selectedJets_phi.clear();
	m_selectedJets_e.clear();

    m_jetsBTagWP60.clear();
    m_jetsBTagWP70.clear();
    m_jetsBTagWP77.clear();
    m_jetsBTagWP85.clear();
    m_jetsBTagWeight.clear();
    m_jetsBTagQuantile.clear();

    std::fill(m_eventBTagSF60_Syst.begin(), m_eventBTagSF60_Syst.end(), 1.0);
    std::fill(m_eventBTagSF70_Syst.begin(), m_eventBTagSF70_Syst.end(), 1.0);
    std::fill(m_eventBTagSF77_Syst.begin(), m_eventBTagSF77_Syst.end(), 1.0);
    std::fill(m_eventBTagSF85_Syst.begin(), m_eventBTagSF85_Syst.end(), 1.0);
    std::fill(m_eventBTagSFContinuous_Syst.begin(), m_eventBTagSFContinuous_Syst.end(), 1.0);
    std::fill(m_eventBTagIneffSFContinuous_Syst.begin(), m_eventBTagIneffSFContinuous_Syst.end(), 1.0);
    std::fill(m_jetJVTSF_Syst.begin(), m_jetJVTSF_Syst.end(), 1.0);
    std::fill(m_jetfJVTSF_Syst.begin(), m_jetfJVTSF_Syst.end(), 1.0);
}
void HmumuMiniTree::ClearElectronsUser(const std::string elecName){
    m_selectedElectronsPos_pt.clear();
	  m_selectedElectronsPos_eta.clear();
	  m_selectedElectronsPos_phi.clear();
	  m_selectedElectronsPos_e.clear();
	  m_selectedElectronsPos_veto.clear();
	  m_selectedElectronsPos_iso.clear();

    m_selectedElectronsNeg_pt.clear();
	  m_selectedElectronsNeg_eta.clear();
	  m_selectedElectronsNeg_phi.clear();
	  m_selectedElectronsNeg_e.clear();
	  m_selectedElectronsNeg_veto.clear();
	  m_selectedElectronsNeg_iso.clear();

    m_selectedElectronsPos_TruthOrigin.clear();
    m_selectedElectronsPos_TruthType.clear();
    m_selectedElectronsNeg_TruthOrigin.clear();
    m_selectedElectronsNeg_TruthType.clear();

    std::fill(m_electronPIDSF_Syst.begin(), m_electronPIDSF_Syst.end(), 1.0);
    std::fill(m_electronIsoSF_Syst.begin(), m_electronIsoSF_Syst.end(), 1.0);
    std::fill(m_electronQualSF_Syst.begin(), m_electronQualSF_Syst.end(), 1.0);
}
void HmumuMiniTree::ClearMuonsUser(const std::string muonName) {
    m_selectedMuonsPos_pt.clear();
	  m_selectedMuonsPos_eta.clear();
	  m_selectedMuonsPos_phi.clear();
	  m_selectedMuonsPos_e.clear();
	  m_selectedMuonsPos_veto.clear();
	  m_selectedMuonsPos_iso.clear();

    m_selectedMuonsNeg_pt.clear();
	  m_selectedMuonsNeg_eta.clear();
	  m_selectedMuonsNeg_phi.clear();
	  m_selectedMuonsNeg_e.clear();
	  m_selectedMuonsNeg_veto.clear();
	  m_selectedMuonsNeg_iso.clear();

    std::fill(m_muonQualSF_Syst.begin(), m_muonQualSF_Syst.end(), 1.0);
    std::fill(m_muonIsoSF_Syst.begin(), m_muonIsoSF_Syst.end(), 1.0);
    m_selectedMuonsPos_TruthOrigin.clear();
    m_selectedMuonsPos_TruthType.clear();
    m_selectedMuonsNeg_TruthOrigin.clear();
    m_selectedMuonsNeg_TruthType.clear();

    m_TrigMatch_HLT_mu26_ivarmedium = false;
    m_vec_TrigMatch_HLT_mu26_ivarmedium.clear();
    m_TrigMatch_HLT_mu20_iloose_L1MU15 = false;
    m_vec_TrigMatch_HLT_mu20_iloose_L1MU15.clear();
    m_TrigMatch_HLT_mu50 = false;
    m_vec_TrigMatch_HLT_mu50.clear();

    //m_selectedJets_TruthOrigin.clear();
    //m_selectedJets_TruthType.clear();

    for (auto& v : m_muonTrigHLT26SF_Syst) {
       v.clear();
    }
    for (auto& v : m_muonTrigHLT26Eff_Syst) {
       v.clear();
    }
    for (auto& v : m_muonTrigHLT20SF_Syst) {
       v.clear();
    }
    for (auto& v : m_muonTrigHLT20Eff_Syst) {
       v.clear();
    }
    
    m_muonPt.clear();
}

