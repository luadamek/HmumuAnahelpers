// Hmumu Analysis for Run2

// c++ include(s):
#include <iostream>
#include <typeinfo>
#include <sstream>

// EL include(s):
#include <EventLoop/Job.h>
#include <EventLoop/Worker.h>

// EDM include(s):
#include "AthContainers/ConstDataVector.h"
#include "InDetTrackSelectionTool/InDetTrackSelectionTool.h"

#include <xAODJet/JetContainer.h>
#include <xAODJet/JetAuxContainer.h>
// package include(s):
#include "xAODEventInfo/EventInfo.h"
#include "xAODAnaHelpers/HelperFunctions.h"
#include "HmumuAnahelpers/HmumuForwardJetSelection.h"
#include "AsgTools/MessageCheck.h"

// ROOT include(s):
#include "TFile.h"
#include "TObjArray.h"
#include "TObjString.h"

// this is needed to distribute the algorithm to the workers
ClassImp(HmumuForwardJetSelection)


HmumuForwardJetSelection :: HmumuForwardJetSelection (std::string className) :
    Algorithm(className)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
  Info("HmumuForwardJetSelection()", "Calling constructor");

}

HmumuForwardJetSelection::~HmumuForwardJetSelection() {}

EL::StatusCode HmumuForwardJetSelection :: setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.

  Info("setupJob()", "Calling setupJob");

  job.useXAOD ();
  xAOD::Init( "HmumuForwardJetSelection" ).ignore(); // call before opening first file

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode HmumuForwardJetSelection :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  Info("histInitialize()", "Calling histInitialize");
  ANA_CHECK( xAH::Algorithm::algInitialize());
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode HmumuForwardJetSelection :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed

  Info("fileExecute()", "Calling fileExecute");

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode HmumuForwardJetSelection :: changeInput (bool /*firstFile*/)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.

  Info("changeInput()", "Calling changeInput");

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode HmumuForwardJetSelection :: initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.
  Info("initialize()", "Initializing HmumuForwardJetSelection" );

  m_event = wk()->xaodEvent();
  m_store = wk()->xaodStore();

  
  TFile *file     = wk()->getOutputFile ("cutflow");
  m_jet_cutflowHist_1 = (TH1D*)file->Get("cutflow_jets_1");

  m_fwd_selection_bin = m_jet_cutflowHist_1->GetXaxis()->FindBin("PassForwardSelection");


  return EL::StatusCode::SUCCESS;
}



EL::StatusCode HmumuForwardJetSelection :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  if(m_debug) Info("execute()", "Applying Forward Jet Object Selection... ");
  ConstDataVector<xAOD::JetContainer>* inJets(nullptr);
  ConstDataVector<xAOD::JetContainer>*  outJets(nullptr);

  // if input comes from xAOD, or just running one collection,
  // then get the one collection and be done with it
  //
  if ( m_inputAlgoSystNames.empty() ) {

    // this will be the collection processed - no matter what!!
    //
    outJets = new ConstDataVector<xAOD::JetContainer>(SG::VIEW_ELEMENTS);
    const xAOD::EventInfo* eventInfo(nullptr);

    ANA_CHECK( HelperFunctions::retrieve(eventInfo, m_eventInfoContainerName, m_event, m_store, msg()) );
    ANA_CHECK( HelperFunctions::retrieve(inJets, m_inContainerName, m_event, m_store, msg()) );
 
    selectJets(inJets, outJets);

    ANA_CHECK(m_store->record(outJets, m_outContainerName ));

  }

  else{
    // get vector of string giving the syst names of the upstream algo from TStore (rememeber: 1st element is a blank string: nominal case!)
    //
    std::vector< std::string >* systNames(nullptr);
    ANA_CHECK( HelperFunctions::retrieve(systNames, m_inputAlgoSystNames, 0, m_store, msg()) );

    const xAOD::EventInfo* eventInfo(nullptr);
    ANA_CHECK( HelperFunctions::retrieve(eventInfo, m_eventInfoContainerName, m_event, m_store, msg()) );
    // prepare a vector of the names of CDV containers for usage by downstream algos
    // must be a pointer to be recorded in TStore
    //
    std::vector< std::string >* vecOutContainerNames = new std::vector< std::string >;
    ANA_MSG_DEBUG( " input list of syst size: " << static_cast<int>(systNames->size()) );

    // loop over systematic sets
    //
    for ( auto systName : *systNames ) {
      ANA_MSG_DEBUG( " syst name: " << systName << "  input container name: " << m_inContainerName+systName );
      vecOutContainerNames->push_back( systName );

      ANA_CHECK( HelperFunctions::retrieve(inJets, m_inContainerName + systName, m_event, m_store, msg()) );

      outJets = new ConstDataVector<xAOD::JetContainer>(SG::VIEW_ELEMENTS); 

      selectJets(inJets, outJets);

      ANA_CHECK( m_store->record( outJets, m_outContainerName+systName ));
      ANA_MSG_DEBUG( " syst name: " << systName << "  output container name: " << m_outContainerName+systName );

    } // close loop over syst sets


    ANA_CHECK( m_store->record( vecOutContainerNames, m_outputAlgoSystNames));

  }

  return EL::StatusCode::SUCCESS;
}

void HmumuForwardJetSelection :: selectJets(ConstDataVector<xAOD::JetContainer>* inJets, ConstDataVector<xAOD::JetContainer>* outJets)
{
  for(auto jet : *inJets) {
    if ( (jet->pt() < m_pt_min) && (std::fabs(jet->eta()) < m_eta_max) && std::fabs(jet->eta()) > m_eta_min) continue;
    m_jet_cutflowHist_1->Fill(m_fwd_selection_bin, 1);
    outJets->push_back(jet);
  }
}


EL::StatusCode HmumuForwardJetSelection :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.

  if(m_debug) Info("postExecute()", "Calling postExecute");

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode HmumuForwardJetSelection :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.
  //
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode HmumuForwardJetSelection :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  ANA_CHECK( xAH::Algorithm::algFinalize() );
  return EL::StatusCode::SUCCESS;
}
