#include <HmumuAnahelpers/HmumuOverlapRemover.h>
#include <HmumuAnahelpers/HmumuTreeAlgo.h>
#include <HmumuAnahelpers/HmumuForwardJetSelection.h>
#include <HmumuAnahelpers/HmumuMiniTree.h>
#include <HmumuAnahelpers/HmumuTruthSelector.h>
#include <HmumuAnahelpers/HmumuEventSelection.h>

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#endif

#ifdef __CINT__
#pragma link C++ class HmumuOverlapRemover+;
#pragma link C++ class HmumuTreeAlgo+;
#pragma link C++ class HmumuForwardJetSelection+;
#pragma link C++ class HmumuMiniTree+;
#pragma link C++ class HmumuTruthSelector+;
#pragma link C++ class HmumuEventSelection+;
#endif
