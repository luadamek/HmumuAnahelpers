// EL include(s):
#include <EventLoop/Job.h>
#include <EventLoop/Worker.h>
#include "EventLoop/OutputStream.h"

// EDM include(s):
#include "xAODEventInfo/EventInfo.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"

// package include(s):
#include <xAODAnaHelpers/HelperFunctions.h>
#include <HmumuAnahelpers/HmumuEventSelection.h>

#include "PATInterfaces/CorrectionCode.h"
//#include "AsgTools/StatusCode.h"

// tools
#include "GoodRunsLists/GoodRunsListSelectionTool.h"
#include "PileupReweighting/PileupReweightingTool.h"
#include "TrigConfxAOD/xAODConfigTool.h"
#include "PMGTools/PMGSherpa22VJetsWeightTool.h"

// ROOT include(s):
#include "TFile.h"
#include "TTree.h"
#include "TTreeFormula.h"
#include "TSystem.h"
#include "xAODCore/tools/IOStats.h"
#include "xAODCore/tools/ReadStats.h"

#include "xAODMetaData/FileMetaData.h"

// external tools include(s):
#include "AsgTools/AnaToolHandle.h"


// this is needed to distribute the algorithm to the workers
ClassImp(HmumuEventSelection)

HmumuEventSelection :: HmumuEventSelection () :
    Algorithm("HmumuEventSelection")
{
}


EL::StatusCode HmumuEventSelection :: setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.
  //
  ANA_MSG_INFO( "Calling setupJob");

  job.useXAOD();
  // let's initialize the algorithm to use the xAODRootAccess package
  xAOD::Init("").ignore(); // call before opening first file

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode HmumuEventSelection :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  ANA_MSG_INFO( "Calling histInitialize");
  ANA_CHECK( xAH::Algorithm::algInitialize());

  // write the metadata hist to this file so algos downstream can pick up the pointer
  TFile *file = wk()->getOutputFile ("cutflow");

  // use 1,1,2 so Fill(bin) and GetBinContent(bin) refer to the same bin
  //
  m_cutflowHist  = (TH1D*) file->Get("cutflow");
  m_cutflowHistW = (TH1D*) file->Get("cutflow_weighted");

  m_cutflowHist->GetXaxis()->FindBin("Year Specific Trigger");
  m_cutflowHistW->GetXaxis()->FindBin("Year Specific Trigger");
  // initialise object cutflows, which will be picked by the object selector algos downstream and filled.
  //


  return EL::StatusCode::SUCCESS;
}


EL::StatusCode HmumuEventSelection :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed

  ANA_MSG_INFO( "Calling fileExecute");

  // get TEvent and TStore - must be done here b/c we need to retrieve CutBookkeepers container from TEvent!
  //
  m_event = wk()->xaodEvent();
  m_store = wk()->xaodStore();

  ANA_MSG_DEBUG( "Is MC? " << isMC() );

  triggers2015Vec.clear();
  std::string token;
  std::istringstream ss(m_triggerSelection2015);
  while ( std::getline(ss, token, ',') ) {
      ANA_MSG_DEBUG("Will look for 2015 trigger " + token);
      triggers2015Vec.push_back(token);
  }

  triggers201620172018Vec.clear();
  std::istringstream ss2(m_triggerSelection201620172018);
  while ( std::getline(ss2, token, ',') ) {
      ANA_MSG_DEBUG("Will look for 2016/2017 trigger " + token);
      triggers201620172018Vec.push_back(token);
  }

  
   ANA_CHECK( m_pileup_tool_handle.retrieve());

  return EL::StatusCode::SUCCESS;

}

EL::StatusCode HmumuEventSelection :: changeInput (bool /*firstFile*/)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode HmumuEventSelection :: initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.

  ANA_MSG_INFO( "Initializing ... ");


  return EL::StatusCode::SUCCESS;
}



EL::StatusCode HmumuEventSelection :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  ANA_MSG_DEBUG( "Basic Event Selection");

  // Print every 1000 entries, so we know where we are:
  //
  if ( (m_eventCounter % 1000) == 0 ) {
    ANA_MSG_INFO( "Entry number = " << m_eventCounter);
    ANA_MSG_VERBOSE( "Store Content:");
    if(msgLvl(MSG::VERBOSE)) m_store->print();
    ANA_MSG_VERBOSE( "End Content");
  }
  ++m_eventCounter;

  if (!isMC()) return EL::StatusCode::SUCCESS;
  //------------------
  // Grab event
  //------------------
  const xAOD::EventInfo* eventInfo(nullptr);
  ANA_CHECK( HelperFunctions::retrieve(eventInfo, "EventInfo", m_event, m_store, msg()) );
  static SG::AuxElement::Accessor< std::vector< std::string > >  acc_passedTriggers("passedTriggers");
  unsigned int evt_randomRunNumber = eventInfo->auxdecor<unsigned int>("RandomRunNumber");
  if (evt_randomRunNumber == 0){ ANA_MSG_DEBUG("The event had a random run number of 0. Recomputing"); evt_randomRunNumber = m_pileup_tool_handle->getRandomRunNumber(*eventInfo, false );}

  float mcEventWeight = eventInfo->auxdecor<float>("mcEventWeight");

  if (!acc_passedTriggers.isAvailable(*eventInfo)){
      ANA_MSG_ERROR("Couldn't retrieve passed triggers"); return EL::StatusCode::FAILURE;
  }
  std::vector< std::string > passedTriggers = acc_passedTriggers(*eventInfo);
  bool is2015 = evt_randomRunNumber <= 284484;

  bool pass_event = false;
  std::vector< std::string > triggerVec = is2015 ? triggers2015Vec : triggers201620172018Vec;
  for (auto trigger: triggerVec){
     ANA_MSG_DEBUG("Scanning for " + trigger);
     if (std::find(passedTriggers.begin(), passedTriggers.end(), trigger) != passedTriggers.end()) pass_event = true;
  }

  if (pass_event){
    m_cutflowHist->Fill("Year Specific Trigger", 1);
    m_cutflowHistW->Fill("Year Specific Trigger", mcEventWeight);
    ANA_MSG_DEBUG("Event passed the trigger selection");
  }
  else {
    ANA_MSG_DEBUG("Event failed the trigger selection");
    wk()->skipEvent();
    return EL::StatusCode::SUCCESS;
  }


  bool lar_error = ( eventInfo->errorState( xAOD::EventInfo::LAr ) == xAOD::EventInfo::Error );
  bool tile_error = ( eventInfo->errorState( xAOD::EventInfo::Tile ) == xAOD::EventInfo::Error );
  bool halo = (eventInfo->isEventFlagBitSet( xAOD::EventInfo::Core, 18 ) );
  if (lar_error || tile_error || halo){ANA_MSG_DEBUG("Event failed cleaning cut"); wk()->skipEvent(); return EL::StatusCode::SUCCESS;}
  m_cutflowHist->Fill("Pass Event Cleaning", 1);
  m_cutflowHistW->Fill("Pass Event Cleaning", mcEventWeight);

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode HmumuEventSelection :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode HmumuEventSelection :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input eventsA
  
  triggers2015Vec.clear();
  triggers201620172018Vec.clear();

  ANA_MSG_INFO( "Number of processed events \t= " << m_eventCounter);

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode HmumuEventSelection :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  ANA_CHECK( xAH::Algorithm::algFinalize());
  return EL::StatusCode::SUCCESS;
}
