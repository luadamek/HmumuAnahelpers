infile = "mc16a_bkg.txt"
outfile = infile + "noduplicates"

with open(infile) as f:
    content = f.readlines()

content = [x.rstrip("\n") for x in content]

content = [x.rstrip(" ") for x in content]

unique_content = []
for line in content:
    if line + "\n" not in unique_content:
        unique_content.append(line + "\n")

unique_content[-1] = unique_content[-1].rstrip("\n")
print(unique_content)

out_file = open(outfile, 'a')
for line in unique_content:
    out_file.write(line)
