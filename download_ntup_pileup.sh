lsetup pyAmi
lsetup rucio
python scripts/getPRWFiles.py --filelist ttHmumuFilesmc16e.txt --output_file data/samples/HIGG1D2/prw_files/HIGG1D2_mc16e_prw.root
python scripts/getPRWFiles.py --filelist ttHmumuFilesmc16d.txt --output_file data/samples/HIGG1D2/prw_files/HIGG1D2_mc16d_prw.root
python scripts/getPRWFiles.py --filelist ttHmumuFilesmc16a.txt --output_file data/samples/HIGG1D2/prw_files/HIGG1D2_mc16a_prw.root
