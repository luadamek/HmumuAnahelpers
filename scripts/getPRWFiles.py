import os

import subprocess
import argparse
parser = argparse.ArgumentParser(description='download the ntuple reweighting files for the DAODs in a given file list')
parser.add_argument('--filelist', '-filelist', dest="filelist", type=str, required=True, help='the input list of samples for processing')
parser.add_argument('--output_file', '-output_file', dest="output_file", type=str, required=True, help='a name for the output root prw file')

args = parser.parse_args()
filelist = args.filelist

print("Looking inside file " + filelist)

with open(filelist) as f:
    lines = f.readlines()

datasets = []
for line in lines:
    line = line.rstrip("\n").rstrip(" ")
    if len(line) < 1:
        continue
    if line[0] == "#":
        continue
    if "mc16_13TeV" not in line:
        continue
    datasets.append(line)

dsids = []
output_files = []
for dataset in datasets:
    dsids.append(dataset.split(".")[1])
    output_files.append(dataset.split(".")[-1])

downloaded = []
couldnt_find = []
for dsid, output_file in zip(dsids, output_files):
    command = ["ami", "list", "datasets", "--dataset-number",  dsid, "--type",  "NTUP_PILEUP"]
    print command
    proc = subprocess.Popen(command, stdout=subprocess.PIPE)
    outputs = proc.stdout.read()

    outputs = outputs.split("\n")
    print "file output_file" + output_file

    matching_output = ""
    for output in outputs:
        if output == "":
            continue
        output_file_ntup = output.split(".")[-1]

        print "ntuple output_file " + output_file_ntup

        if dsid in output and output_file[:-6] == output_file_ntup[:-6]:
            if matching_output == "" or int(matching_output[-4:]) < int(output_file_ntup[-4:]):
                print "Found a match with " + output
                matching_output = output

    if matching_output == "":
        couldnt_find.append(dsid)
    else:
        downloaded.append(matching_output)

    print "Downloading file " + matching_output
    command = ["rucio", "download", matching_output]
    #proc = subprocess.Popen(command, stdout=subprocess.PIPE)
    os.system(" ".join(command))
    outputs = proc.stdout.read()

    if not os.path.exists(matching_output):
        couldnt_find.append(matching_output)
        print "WARNING IT LOOKS LIKE THIS DATASET WASNT DOWNLOADED " + matching_output
        continue

    print outputs

    downloaded.append(matching_output)

paths = os.path.split(args.output_file)
import os
if len(paths) > 0 and not os.path.exists(paths[0]): os.makedirs(paths[0])

hadd_command = ["hadd", "-f", args.output_file]

for download in downloaded:
    hadd_command.append(download + "/*root*")

os.system(" ".join(hadd_command))

for d in downloaded:
    os.system("rm -rf {}".format(d))

for entry in couldnt_find:
    print("WARNING COUNLDN'T FIND ENTRY " + entry)





