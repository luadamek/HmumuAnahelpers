#!/usr/bin/env python 
import os

import subprocess
try:
  __version__ = subprocess.check_output(['git','log', '--pretty=format:%h', '-n 1'], cwd=os.path.dirname(os.path.realpath(__file__))).strip()
except:
  print('git not available to extract current tag')
  __version__ = 'private'

import argparse
parser = argparse.ArgumentParser(description='Submit gridjobs for HmumuAnalysis')

parser.add_argument('--user', '-u', dest="user", type=str, required=True, help='Your (CERN id) grid username')
parser.add_argument('--submitDir', dest="submitDir", type=str, default='submitDir', help='dir to store the output')
parser.add_argument('--overwrite',  action='store_true', default=False, help='overwrite previous submitDir')
parser.add_argument('--test', action='store_true', default=False ,help='If true, do not run grid submission command. Instead, print the command for debugging.')
parser.add_argument('--FileList', dest="FileList", required=True, type=str, help='the .txt file containing all dataset names')
parser.add_argument('--config', dest="config", required=True, type=str, help="the file containing the configuration")
parser.add_argument('--descriptor', dest="descriptor", required=True, type=str, help="a string that will be appended to the output dataset")
parser.add_argument('--NFilesPerJob', dest="NFilesPerJob", default=False, required=False, type=int, help="require NFilesPerJob files per job. This is a good way to make your grid jobs a lot faster")
parser.add_argument('--NGBPerJob', dest="NGBPerJob", default=0, required=False, type=int, help="")

args = parser.parse_args()

FileList=args.FileList
ngb = args.NGBPerJob

samples = []
try:
  samples.extend([sample.rstrip('\n') for sample in open(os.path.expandvars(FileList))])
except IOError, e:
  print e
  exit()

for sample in samples:

  split = sample.split('.')
  dsid = split[1]
  tag = split[4]
  esrTags = split[5].rstrip(" ")

  sampleTag = dsid + '.' + tag + '.' + esrTags.rstrip(" ")
  optGridOutputSampleName = 'user.{0:s}.{1:s}'.format(args.user, sampleTag)

  config = os.path.expandvars(args.config)

  command = 'xAH_run.py --files {0:s} --inputRucio --config {1:s} --submitDir {2:s}'.format(sample, config, args.submitDir +  dsid  + esrTags)

  #overwrite the output directory, if it already exists
  if args.overwrite:
    command += ' --force'

  command += ' prun '

  if (args.NFilesPerJob):
      command +=' --optGridNFilesPerJob ' + str(args.NFilesPerJob) + " "
  if ngb != 0:
      command +=' --optGridNGBPerJob ' + str(ngb)
  else:
      command +=' --optGridNGBPerJob MAX'

  if args.test:
      command += ' --optGridNFiles 1'
  command += ' --optGridOutputSampleName {}'.format(optGridOutputSampleName) + "_"+ args.descriptor

  command += " --optGridExcludedSite ANALY_RAL_ECHO,ANALY_DESY-HH "

  print('submitting {0:s}'.format(sample))
  print('running: {0:s}'.format(command))
  subprocess.call(command, shell=True)
