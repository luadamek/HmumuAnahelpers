import os

from subprocess import call
call(["ls", "-l"])

import argparse
parser = argparse.ArgumentParser(description='Hadd together files downloaded from the grid')

parser.add_argument('--tag', '-t', dest="tag", type=str, required=True, help='The tag of the directories with the files that you want to hadd together')
parser.add_argument('--dir', '-dir', dest="dir", type=str, required=True, help='The top most directory containing the folders and files that should be hadded together')


args = parser.parse_args()
directory = args.dir
tag = args.tag

print("Looking inside directory " + directory)
print("Inside directory " + directory+ " I will hadd together the files in folders with " + tag + " in their name")


for header, all_folders, files in os.walk(directory):
    for folder in all_folders:
        if not tag in folder:
            continue
        print folder

        command = "hadd " + folder + " " + header + "/" + folder + "/*.root*"
        os.system(command)





